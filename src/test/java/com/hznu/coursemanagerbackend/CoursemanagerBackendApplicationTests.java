package com.hznu.coursemanagerbackend;

import com.hznu.coursemanagerbackend.modules.sys.bean.ExerciseDetail;
import com.hznu.coursemanagerbackend.modules.sys.dao.ExerciseInfoDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseInfo;
import com.hznu.coursemanagerbackend.modules.sys.service.ExerciseInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CoursemanagerBackendApplicationTests {

    @Autowired
    private ExerciseInfoDao exerciseInfoDao;

    @Autowired
    private ExerciseInfoService exerciseInfoService;

    @Test
    public void contextLoads() {
        ExerciseDetail temp = new ExerciseDetail();
        temp.setIsready(1);
        List<ExerciseDetail> list = exerciseInfoService.findByCondition(temp);
        System.out.println("ZONGSHU:" + list.size());
        for (ExerciseDetail item : list) {
            System.out.println(item.toString());
        }
    }

}
