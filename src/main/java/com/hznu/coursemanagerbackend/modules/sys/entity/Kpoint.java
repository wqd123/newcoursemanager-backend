package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;

import java.io.Serializable;
import java.util.List;

import static org.bouncycastle.asn1.x500.style.RFC4519Style.o;

/**
 * <p>
 * <p>
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class Kpoint implements Comparable<Kpoint>{

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private Integer parentid;
    private Integer courseid;
    private Integer number;

    //附加字段
    @TableField(exist = false)
    private List<Kpoint> children;

    public List<Kpoint> getChildren() {
        return children;
    }

    public void setChildren(List<Kpoint> children) {
        this.children = children;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public Integer getCourseid() {
        return courseid;
    }

    public void setCourseid(Integer courseid) {
        this.courseid = courseid;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Kpoint{" +
                ", id=" + id +
                ", name=" + name +
                ", parentid=" + parentid +
                ", courseid=" + courseid +
                ", number=" + number +
                "}";
    }

    //@Override
    public int compareTo(Kpoint kpoint) {
        return this.getNumber()-kpoint.getNumber();
    }

}
