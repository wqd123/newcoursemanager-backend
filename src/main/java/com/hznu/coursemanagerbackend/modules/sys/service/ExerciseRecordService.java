package com.hznu.coursemanagerbackend.modules.sys.service;

import com.hznu.coursemanagerbackend.modules.sys.entity.AnswerRecord;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseRecord;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface ExerciseRecordService extends IService<ExerciseRecord> {

    void addOne(AnswerRecord answerRecord);

}
