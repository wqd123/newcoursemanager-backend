package com.hznu.coursemanagerbackend.modules.sys.controller;

import com.hznu.coursemanagerbackend.modules.sys.entity.Question;
import com.hznu.coursemanagerbackend.modules.sys.service.SaveQuestionService;
import com.hznu.coursemanagerbackend.myBeans.R;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 *  Controller类 删除了flying saucer相关
 * </p>
 *
 * @author weimeng
 * @since 2019/3/4
 */
@Api(description = "生成PDF相关接口",value = "getPdf")
@RestController
@RequestMapping("/getPdf")
public class GetHtmlController {
    public SaveQuestionService saveQuestionService;

    String totalName;
    String firstLine,secondLine;
    @Value("${server.path}")
    String localhostPath;
    @Autowired
    public GetHtmlController(SaveQuestionService saveQuestionService) {
        this.saveQuestionService = saveQuestionService;
    }
    @ApiOperation("添加一道题目")
    @PostMapping("/addQuestion")
    public R getQuestion(@RequestBody Question question){
        saveQuestionService.add(question);
        return R.ok("成功添加一道题目");
    }
    @ApiOperation("添加一组题目 即 题目的数组")
    @PostMapping("/addQuestions")
    public R getQuestions(@RequestBody LinkedList<Question> createpaper){
        saveQuestionService.questions.clear();
        saveQuestionService.questions=createpaper;
        return R.ok("成功添加所有题目的列表");
    }
    @ApiOperation("得到所有题目")
    @PostMapping("/getAllQuestions")
    public List<Question> getQeustions(){
        return saveQuestionService.questions;
    }
    @ApiOperation("清空后端题目缓存")
    @PostMapping("/clearQuestions")
    public R clearQuestions(){
        saveQuestionService.questions.clear();
        return R.ok("清除成功");
    }

    @ApiOperation("添加内容用于生成试卷抬头xxx年 xx 学期 xx..")
    @GetMapping("/addTotalName/{totalName}")
    public R addTotalName(@PathVariable("totalName") String totalName){
        this.totalName = totalName;
        System.out.println("totalName=="+totalName+"here we are");
        try{
            System.out.println("totalName"+totalName);
            if(totalName.length()>50){
                return R.error("试卷标题长度不可大于30个字符");
            }
            int index = totalName.length()>25?25:totalName.length();
            firstLine = totalName.substring(0,index);
            secondLine = totalName.length()>25?totalName.substring(index):"";
        }catch(Exception e){
            e.printStackTrace();
            this.totalName = "请在标题内添加《》 书名号";
        }
        return R.ok("考试科目名称添加成功");
    }
    @ApiOperation("返回试卷抬头（分两行）所以返回是一个数组")
    @PostMapping("/getTotalName")
    public List<String> getTotalName(){
        System.out.println("getTotalName here we are");
        List<String> titles = new ArrayList<>();
        titles.add(firstLine);
        titles.add(secondLine);
        System.out.println("firstLine=="+firstLine+";;;secondLine=="+secondLine);
        return titles;
    }
    @ApiOperation("得到当前域名地址")
    @PostMapping("/getLocalhostPath")
    public String getLocalhostPath(){
        return this.localhostPath;
    }

}
