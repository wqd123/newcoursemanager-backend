package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerkPoint;
import com.hznu.coursemanagerbackend.modules.sys.entity.Kpoint;
import com.hznu.coursemanagerbackend.modules.sys.dao.KpointDao;
import com.hznu.coursemanagerbackend.modules.sys.service.ExerkPointService;
import com.hznu.coursemanagerbackend.modules.sys.service.KpointService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class KpointServiceImpl extends ServiceImpl<KpointDao, Kpoint> implements KpointService {

    @Autowired
    private KpointService kpointService;

    @Autowired
    private ExerkPointService exerkPointService;

    @Override
    public PageUtils queryPage(Map<String,Object> params)
    {
        String keyword = (String)params.get("keyword");
        Page<Kpoint> page = this.selectPage(
                new Query<Kpoint>(params).getPage(),
                new EntityWrapper<Kpoint>().eq(keyword!=null,"courseid",keyword)
        );
        return new PageUtils(page);
    }

    @Override
    public List<Kpoint> queryListByPid(Integer pid) {
        List<Kpoint> kpointList = kpointService.selectList(new EntityWrapper<Kpoint>().eq("parentid",pid));
        Collections.sort(kpointList);
        if (kpointList.size()==0){
            return null;
        }else {
            for (Kpoint kpoint : kpointList) {
                List<Kpoint> childList = queryListByPid(kpoint.getId());
                kpoint.setChildren(childList);
            }
        }
        return kpointList;
    }

    @Override
    public void removeById(Integer id){
        List<Kpoint> kpointList = kpointService.selectList(new EntityWrapper<Kpoint>().eq("parentid",id));
        if(kpointList.size()!=0) {
            for (Kpoint kpoint : kpointList) {
                removeById(kpoint.getId());
            }
            List<ExerkPoint> exerkPointList = exerkPointService.selectList(new EntityWrapper<ExerkPoint>().eq("kpointid",id));
            for(ExerkPoint exerkPoint : exerkPointList){
                exerkPointService.deleteById(exerkPoint.getId());
            }
            this.deleteById(id);
        }else{
            List<ExerkPoint> exerkPointList = exerkPointService.selectList(new EntityWrapper<ExerkPoint>().eq("kpointid",id));
            for(ExerkPoint exerkPoint : exerkPointList){
                exerkPointService.deleteById(exerkPoint.getId());
            }
            this.deleteById(id);
            return;
        }

    }

}
