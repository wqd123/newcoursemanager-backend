package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.validator.ValidatorUtils;
import com.hznu.coursemanagerbackend.common.validator.group.AddGroup;
import com.hznu.coursemanagerbackend.modules.sys.bean.ClassFindBean;
import com.hznu.coursemanagerbackend.modules.sys.entity.*;
import com.hznu.coursemanagerbackend.modules.sys.entity.Class;
import com.hznu.coursemanagerbackend.modules.sys.service.*;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/class")
@Api(description = "班级接口",value = "class")
public class ClassController {

    @Autowired
    ClassService classService;

    @Autowired
    MajorService majorService;

    @Autowired
    SchoolService schoolService;

    @Autowired
    DepartmentService departmentService;

    @Autowired
    TeacherInfoService teacherInfoService;

    @Autowired
    UserService userService;

    @Autowired
    UniversityService universityService;

    @Autowired
    StudentInfoService studentInfoService;

    /**
     * @author Wqd
     * @since 2018-12-1
     */
    @ApiOperation("添加班级")
    @PostMapping("/add")
    public R add(@RequestBody Class clazz){
        try{
            if(classService.findByName(clazz).size()!=0){
                return R.error("已存在该班级");
            }else {
                Major major = majorService.findById(clazz.getMajorid());
                clazz.setDepartmentid(major.getDepartmentid());
                clazz.setSchoolid(major.getSchoolid());
                clazz.setUniversityid(major.getUniversityid());
                classService.insert(clazz);
            }
            return R.ok();
        }catch (Exception e){
            e.printStackTrace();
            return R.error();
        }
    }

    /**
     * @author Wqd
     * @since 2018-12-2
     */
    @ApiOperation("根据专业查找班级")
    @PostMapping("/findclassbymajorid/{majorid}")
    public R findclassbymajorid(@PathVariable Integer majorid) {
        try {
            List<Class> list = classService.selectList(new EntityWrapper<Class>().eq("majorid",majorid));
            return R.ok().put("data", list);
        } catch (Exception e) {
            e.printStackTrace();
            return R.error();
        }
    }

    /**
     * @Description:关键字查找班级
     * @param:
     * @return:
     * @author: Wqd
     * @Date: 2019/3/11
     */
    @ApiOperation("关键字查找班级")
    @PostMapping("/findClassByClassname")
    public R findClassByCondition(@RequestBody ClassFindBean classFindBean) {
        try {
            String name = classFindBean.getName();
            if(name!=null && !"".equals(name)){
                List<Class> classList = classService.selectBykeyword(name);
                return R.ok().put("data", classList);
            }else{
                Integer teacherid = classFindBean.getTeacherid();
                Assert.notNull(teacherid,"参数为空");
                TeacherInfo teacherInfo = teacherInfoService.selectById(teacherid);
                User user = userService.selectById(teacherInfo.getUserid());
                List<Class> classList1 = classService.selectList(new EntityWrapper<Class>().eq("universityid",user.getUniversityid()));
                return R.ok().put("data", classList1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.error();
        }
    }

    /**
     * @author Wqd
     * @since 2019-3-28
     */
    @ApiOperation("删除班级")
    @GetMapping("/removeclass/{id}")
    public R removeclass(@PathVariable Integer id) {
        try {
            if(studentInfoService.selectList(new EntityWrapper<StudentInfo>().eq("classid",id)).size()==0){
                return R.ok("删除成功").put("data", classService.deleteById(id));
            }else{
                return R.error("班级下有学生，不能删除");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("程序异常");
        }
    }

    /**
     * @author Wqd
     * @since 2019-3-31
     */
    @ApiOperation("修改专业信息")
    @PostMapping("/modify")
    public R modify(@RequestBody Class clazz) {
        ValidatorUtils.validateEntity(clazz, AddGroup.class);
        classService.updateById(clazz);
        return R.ok();
    }

    @ApiOperation("条件查询班级（分页）")
    @PostMapping("/list")
    public R list(@RequestParam Map<String,Object> params)
    {
        PageUtils page = classService.queryPage(params);
        return R.ok().put("page",page);
    }

    @ApiOperation("根据专业d筛选班级（分页）")
    @PostMapping("/findClassByMajorId")
    public R findClassByMajorId(@RequestParam Map<String,Object> params)
    {
        PageUtils page = classService.queryPageByMajorId(params);
        return R.ok().put("page",page);
    }

}

