package com.hznu.coursemanagerbackend.modules.sys.entity;

public class AjaxResponseBody {
    private String msg;
    private String html;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}
