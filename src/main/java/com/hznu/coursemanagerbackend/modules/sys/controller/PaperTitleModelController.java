package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.entity.PaperTitleModel;
import com.hznu.coursemanagerbackend.modules.sys.service.PaperTitleModelService;
import com.hznu.coursemanagerbackend.myBeans.R;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2019-04-02
 */
@Api(description = "试卷标题模板接口类",value = "paperTitleModel")
@RestController
@RequestMapping("/paperTitleModel")
public class PaperTitleModelController {
    @Autowired
    PaperTitleModelService paperTitleModelService;
    @ApiOperation("对应试卷标题模板的 新增")
    @PostMapping("/add")
    public R add(@RequestBody PaperTitleModel paperTitleModel){
        //System.out.println(paperTitleModel.toString());
        try{
            System.out.println("tostring");
            paperTitleModelService.insert(paperTitleModel);
        }catch(Exception e){
            e.printStackTrace();
        }
        return R.ok();
    }

    @ApiOperation("对应标题模板 删除 按钮 按id删除")
    @GetMapping("/deleteById/{id}")
    public R deleteById(@PathVariable Integer id){
        try{
            paperTitleModelService.deleteById(id);
        }catch(Exception e){
            e.printStackTrace();
            return R.error();
        }
        return R.ok();
    }

    @ApiOperation("对应试卷标题模板的 整体跟新")
    @PostMapping("/update")
    public R update(@RequestBody PaperTitleModel paperTitleModel){
        paperTitleModelService.updateAllColumnById(paperTitleModel);
        return R.ok();
    }

    @ApiOperation("对应试卷标题模板的 部分跟新")
    @PostMapping("/updatePart")
    public R updatePart(@RequestBody PaperTitleModel paperTitleModel){
        paperTitleModelService.updateById(paperTitleModel);
        return R.ok();
    }

    @ApiOperation("对应试卷标题模板的 按id 查找")
    @GetMapping("/selectById/{id}")
    public R selectById(@PathVariable Integer id){
        PaperTitleModel paperTitleModel= paperTitleModelService.selectById(id);
        return R.ok().put("data",paperTitleModel);
    }

    @ApiOperation("用于展示所有的标题模板 并不分页")
    @GetMapping("/getAll")
    public R getAll(){
        List<PaperTitleModel> paperTitleModels= paperTitleModelService.selectList(new EntityWrapper<>());
        return R.ok().put("data",paperTitleModels);
    }

    @ApiOperation("根据keyword（可为空） 返回 标题模板 页 keyword --> name")
    @PostMapping("/titlePage")
    public R customerPage(@RequestParam Map<String,Object> params) {
        PageUtils page = paperTitleModelService.queryPage(params);
        return R.ok().put("page", page);
    }
}

