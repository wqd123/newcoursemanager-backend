package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.bean.CourseTeachDetail;
import com.hznu.coursemanagerbackend.modules.sys.entity.*;
import com.hznu.coursemanagerbackend.modules.sys.service.*;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.validation.constraints.Pattern;
import java.util.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/courseTeach")
@Api(description = "课程教师关联接口",value = "courseTeach")
public class CourseTeachController extends AbstractController {

    @Autowired
    CourseTeachService courseTeachService;
    @Autowired
    CourseService courseService;
    @Autowired
    CourseStudentService courseStudentService;
    @Autowired
    TeacherInfoService teacherInfoService;
    @Autowired
    ExamCourseteachService examCourseteachService;

    /**
     * @Description:认领课程
     * @param:
     * @return:
     * @author: TateBrown
     * @Date: 2018/12/4
     */
    @ApiOperation("教师认领课程")
    @PostMapping("/add")
    public R add(@RequestBody CourseTeach courseTeach) {
        try {
            courseTeachService.insert(courseTeach);
            return R.ok("认领成功");
        } catch (Exception e) {
            logger.error(e.toString());
            return R.error();
        }
    }

    /**
     * @author: DragonDoor
     * @Date: 2018/12/5 12:19
     * @Description:
     */
    @ApiOperation("条件查询教师认领课程（分页）")
    @PostMapping("/list")
    public R list(@RequestParam Map<String,Object> params)
    {
        PageUtils page = courseTeachService.queryPage(params);
        return R.ok().put("page",page);
    }

    /**
     * @author: DragonDoor
     * @Date: 2018/12/5 12:21
     * @Description:
     */
    @ApiOperation("更新教师认领课程信息")
    @PostMapping("/modify")
    public R modify(@RequestBody CourseTeach courseTeach)
    {
        courseTeachService.modify(courseTeach);
        return R.ok();
    }

    /**
     * @author: DragonDoor
     * @Date: 2018/12/19 13:30
     * @Description: 
     */
    @ApiOperation("根据课程id查找教授这门课的所有教师")
    @GetMapping("/findTeacherListByCourseid/{courseid}")
    public R findTeacherListByCourseid(@PathVariable Integer courseid){
        try{
            return R.ok().put("course",courseService.selectById(courseid)).put("teachers",courseTeachService.findTeacherListByCourseid(courseid));
        }catch (Exception e){
            e.printStackTrace();
            return R.error();
        }
    }

    /**
     * @author Wqd
     * @since 2018-12-26
     */
    @ApiOperation("根据授课id查找课程id")
    @PostMapping("/findCourseIdByCourseTeachId/{courseTeachid}")
    public R findCourseIdByCourseTeachId(@PathVariable Integer courseTeachid){
        try{
            CourseTeach courseTeach = courseTeachService.selectById(courseTeachid);
            return R.ok().put("courseid",courseTeach.getCourseid());
        }catch (Exception e){
            e.printStackTrace();
            return R.error();
        }
    }

    /**
     * @author Wqd
     * @since 2019-3-29
     */
    @ApiOperation("删除授课班")
    @GetMapping("/removecourseTeach/{id}")
    public R removecourseTeach(@PathVariable Integer id) {
        try {
            if(examCourseteachService.selectCount(new EntityWrapper<ExamCourseteach>().eq("courseteachid",id))==0){
                List<CourseStudent> courseStudentList =  courseStudentService.selectList(new EntityWrapper<CourseStudent>().eq("courseteachid",id));
                for(CourseStudent courseStudent : courseStudentList){
                    courseStudentService.deleteById(courseStudent.getId());
                }
                courseTeachService.deleteById(id);
                return R.ok("删除成功");
            }else{
                return R.error("该授课班已安排过考试，不能删除");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("程序异常");
        }
    }

}

