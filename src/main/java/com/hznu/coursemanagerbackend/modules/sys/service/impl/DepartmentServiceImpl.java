package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.common.utils.BeanUtils;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.entity.Department;
import com.hznu.coursemanagerbackend.modules.sys.dao.DepartmentDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.Major;
import com.hznu.coursemanagerbackend.modules.sys.entity.School;
import com.hznu.coursemanagerbackend.modules.sys.service.DepartmentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hznu.coursemanagerbackend.modules.sys.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentDao, Department> implements DepartmentService {

    @Autowired
    SchoolService schoolService;

    @Autowired
    DepartmentService departmentService;

    @Override
    public Department findById(Integer departmentid) {
        Department department = this.selectById(departmentid);
        return department;
    }

    @Override
    public List<Department> findByName(Department department) {
        List<Department> departments = this.selectList(new EntityWrapper<Department>().eq("name",department.getName()));
        return departments;
    }

    @Override
    public List<Department> findBySchoolid(Integer schoolid){
        List<Department> department = this.selectList(new EntityWrapper<Department>().eq("schoolid",schoolid));
        return department;
    }

    @Override
    public PageUtils queryPage(Map<String,Object> params)
    {
        String keyword = (String)params.get("keyword");
        Page<Department> page = this.selectPage(
                new Query<Department>(params).getPage(),
                new EntityWrapper<Department>().allEq(BeanUtils.convertBeanToMap(new Department(),params)).andNew(keyword!=null,null).
                        like(keyword!=null,"name",keyword)
        );
        for(Department department : page.getRecords())
        {
            School school = schoolService.selectById(department.getSchoolid());
            department.setSchool(school);
        }
        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageBySchoolId(Map<String,Object> params)
    {
        Integer schoolid = Integer.parseInt((String)params.get("schoolid"));
        Page<Department> page = new Query<Department>(params).getPage();
        List<Department> departmentList = departmentService.selectList(new EntityWrapper<Department>().eq("schoolid",schoolid));
        for(Department department : departmentList)
        {
            School school = schoolService.selectById(department.getSchoolid());
            department.setSchool(school);
        }
        page.setRecords(departmentList);
        return new PageUtils(page);
    }

}
