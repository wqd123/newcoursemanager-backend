package com.hznu.coursemanagerbackend.modules.sys.bean;

import com.xuxueli.poi.excel.annotation.ExcelField;
import com.xuxueli.poi.excel.annotation.ExcelSheet;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * @Author: TateBrown
 * @date: 2018/12/10 19:07
 * @describle: 题目导入实体类
 * @return:
 */
@ExcelSheet(name="Sheet1",headColor = HSSFColor.HSSFColorPredefined.LIGHT_GREEN)
public class ExerciseLoadDto {
    @ExcelField(name="题型")
    private String exerciseType;

    @ExcelField(name="题干")
    private String exerciseContent;

    @ExcelField(name="正确答案")
    private String answer;

    @ExcelField(name="答案解析")
    private String comment;

    @ExcelField(name="难易度")
    private String dlevel;

    @ExcelField(name="选项数目")
    private String choosNum;

    @ExcelField(name="A")
    private String chooseA;

    @ExcelField(name="B")
    private String chooseB;

    @ExcelField(name="C")
    private String chooseC;

    @ExcelField(name="D")
    private String chooseD;

    @ExcelField(name="E")
    private String chooseE;

    @ExcelField(name="F")
    private String chooseF;

    @ExcelField(name="G")
    private String chooseG;

    @ExcelField(name="H")
    private String chooseH;

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }



    public String getChooseA() {
        return chooseA;
    }

    public String getChooseB() {
        return chooseB;
    }

    public String getComment() {
        return comment;
    }

    public String getExerciseContent() {
        return exerciseContent;
    }

    public void setChooseA(String chooseA) {
        this.chooseA = chooseA;
    }



    public void setChooseB(String chooseB) {
        this.chooseB = chooseB;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }



    public void setExerciseContent(String exerciseContent) {
        this.exerciseContent = exerciseContent;
    }

    public String getChooseC() {
        return chooseC;
    }



    public void setChooseC(String chooseC) {
        this.chooseC = chooseC;
    }

    public String getChooseD() {
        return chooseD;
    }

    public String getChooseE() {
        return chooseE;
    }

    public String getChooseF() {
        return chooseF;
    }

    public String getChooseG() {
        return chooseG;
    }

    public void setChooseD(String chooseD) {
        this.chooseD = chooseD;
    }

    public void setChooseE(String chooseE) {
        this.chooseE = chooseE;
    }

    public String getChooseH() {
        return chooseH;
    }

    public void setChooseF(String chooseF) {
        this.chooseF = chooseF;
    }

    public void setChooseG(String chooseG) {
        this.chooseG = chooseG;
    }

    public void setChooseH(String chooseH) {
        this.chooseH = chooseH;
    }

    public void setExerciseType(String exerciseType) {
        this.exerciseType = exerciseType;
    }

    public void setDlevel(String dlevel) {
        this.dlevel = dlevel;
    }

    public void setChoosNum(String choosNum) {
        this.choosNum = choosNum;
    }

    public String getChoosNum() {
        return choosNum;
    }

    public String getDlevel() {
        return dlevel;
    }

    public String getExerciseType() {
        return exerciseType;
    }
}
