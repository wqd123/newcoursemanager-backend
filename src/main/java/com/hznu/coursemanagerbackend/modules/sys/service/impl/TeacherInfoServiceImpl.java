package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.hznu.coursemanagerbackend.modules.sys.entity.TeacherInfo;
import com.hznu.coursemanagerbackend.modules.sys.dao.TeacherInfoDao;
import com.hznu.coursemanagerbackend.modules.sys.service.TeacherInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class TeacherInfoServiceImpl extends ServiceImpl<TeacherInfoDao, TeacherInfo> implements TeacherInfoService {


}
