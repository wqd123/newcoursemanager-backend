package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hznu.coursemanagerbackend.common.utils.BeanUtils;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.bean.ExamDetail;
import com.hznu.coursemanagerbackend.modules.sys.dao.AttendexamDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.*;
import com.hznu.coursemanagerbackend.modules.sys.entity.Class;
import com.hznu.coursemanagerbackend.modules.sys.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class AttendexamServiceImpl extends ServiceImpl<AttendexamDao, Attendexam> implements AttendexamService {

    @Autowired
    AttendexamService attendExamService;

    @Autowired
    StudentInfoService studentInfoService;

    @Autowired
    UserService userService;

    @Autowired
    ClassService classService;

    @Autowired
    ExamService examService;

    @Override
    public PageUtils queryPageByExamId(Map<String,Object> params){
        Page<Attendexam> page = this.selectPage(
                new Query<Attendexam>(params).getPage(),
                new EntityWrapper<Attendexam>().allEq(BeanUtils.convertBeanToMap(new Attendexam(),params))
        );
        for(Attendexam attendexam : page.getRecords()){
            StudentInfo studentInfo = studentInfoService.selectById(attendexam.getStudentid());
            User user = userService.selectById(studentInfo.getUserid());
            Class clazz = classService.selectById(studentInfo.getClassid());
            studentInfo.setClazz(clazz);
            studentInfo.setUser(user);
            attendexam.setStudentInfo(studentInfo);
        }
        return new PageUtils(page);
    }

    @Override
    public List<ExamDetail> findExamsByStudentid(Integer studentid)
    {
        List<Attendexam> attendexams = this.selectList(new EntityWrapper<Attendexam>().eq("studentid",studentid));
        List<ExamDetail> examDetails = new LinkedList<>();
        for(Attendexam attendexam : attendexams)
        {
            ExamDetail examDetail = new ExamDetail();
            Exam exam=examService.selectById(attendexam.getExamid());
            examDetail.setAttendExamid(attendexam.getId());
            examDetail.setDuration(exam.getDuration());
            examDetail.setExamdate(exam.getExamdate());
            examDetail.setExampaperid(exam.getExampaperid());
            examDetail.setFinishtime(exam.getFinishtime());
            examDetail.setId(exam.getId());
            examDetail.setInvigilator1(exam.getInvigilator1());
            examDetail.setInvigilator2(exam.getInvigilator2());
            examDetail.setName(exam.getName());
            examDetail.setNop(exam.getNop());
            examDetail.setPlace(exam.getPlace());
            examDetail.setStarttime(exam.getStarttime());
            examDetail.setState(exam.getState());
            examDetails.add(examDetail);
        }
        return examDetails;
    }

    @Override
    public List<Attendexam> findStudentByExamidAndCourseteachid(Integer examid,Integer courseteachid)
    {
        List<Attendexam> attendexamList = this.baseMapper.selectByExamidAndCourseteachid(examid,courseteachid);
        for(Attendexam attendexam:attendexamList)
        {
            attendexam.setStudentInfo(studentInfoService.studnetDetail(attendexam.getStudentid()));
        }
        return attendexamList;
    }

    @Override
    public void initRemain(Exam exam)
    {
        List<Attendexam> attendexamList = attendExamService.selectList(new EntityWrapper<Attendexam>().eq("examid",exam.getId()));
        for(Attendexam attendexam : attendexamList)
        {
            attendexam.setRemain(exam.getDuration()*60);
        }
        if(!attendexamList.isEmpty())
        {
            attendExamService.updateBatchById(attendexamList);
        }
    }

    @Override
    public Attendexam remainReduce(Integer id,Integer sec)
    {
        Attendexam attendexam = this.selectById(id);
        int remain = attendexam.getRemain();
        remain -= sec;
        if(remain <= 0)
        {
            attendexam.setRemain(0);
            attendexam.setEndtime(new Date(System.currentTimeMillis()));
        }else
        {
            attendexam.setRemain(remain);
        }
        this.updateById(attendexam);
        return attendexam;
    }
}
