package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.hznu.coursemanagerbackend.myBeans.TreeSupport;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class School extends TreeSupport implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private Integer universityid;
    private Integer status;
    private String userdefaultpwd;

    //附加字段
    @TableField(exist = false)
    private List<Department> children;
    @TableField(exist = false)
    private final int adminrank = 2;


    public List<Department> getChildren() {
        return children;
    }

    public void setChildren(List<Department> children) {
        this.children = children;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUniversityid() {
        return universityid;
    }

    public void setUniversityid(Integer universityid) {
        this.universityid = universityid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUserdefaultpwd() {
        return userdefaultpwd;
    }

    public void setUserdefaultpwd(String userdefaultpwd) {
        this.userdefaultpwd = userdefaultpwd;
    }

    public int getAdminrank() {
        return adminrank;
    }

    @Override
    public String toString() {
        return "School{" +
        ", id=" + id +
        ", name=" + name +
        ", universityid=" + universityid +
        ", status=" + status +
        ", userdefaultpwd=" + userdefaultpwd +
        "}";
    }
}
