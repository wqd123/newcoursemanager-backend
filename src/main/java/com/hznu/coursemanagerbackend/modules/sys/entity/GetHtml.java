package com.hznu.coursemanagerbackend.modules.sys.entity;

public class GetHtml {
    String html;

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}
