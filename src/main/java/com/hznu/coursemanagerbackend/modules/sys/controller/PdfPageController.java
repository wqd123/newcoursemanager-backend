package com.hznu.coursemanagerbackend.modules.sys.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
@Api(description = "生成PDF相关html页面地址",value = "pdfPage")
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@Controller
@RequestMapping("/pdfPage")
public class PdfPageController {
    /**
     * @author WM
     * @since 2018-12-11
     */
    @ApiOperation("我原本想的生成pdf的页面，到时候可以用陈宇航的换掉")
    @GetMapping("/pdf")
    public String pdfPage() {
        return "pdfpage";
    }
    @ApiOperation("模式一试卷样卷生成")
    @GetMapping("/noscore")
    public String noscore(){
        return "noscore";
    }
    @ApiOperation("模式二试卷样卷生成 有分数框")
    @GetMapping("/hasscore")
    public String hasscore(){
        return "hasscore";
    }
}
