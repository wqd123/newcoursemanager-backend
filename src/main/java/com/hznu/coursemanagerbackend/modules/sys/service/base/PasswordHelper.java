package com.hznu.coursemanagerbackend.modules.sys.service.base;

import com.hznu.coursemanagerbackend.common.cryptolib.CryptoApp;
import com.hznu.coursemanagerbackend.common.cryptolib.PsdProcRlt;
import com.hznu.coursemanagerbackend.common.utils.Utils;
import com.hznu.coursemanagerbackend.modules.sys.entity.User;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by TAO on 2015/10/3.
 */
@Service
public class PasswordHelper { //UserServiceImpl中createUser会用到。

    private RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();

    @Value("${password.algorithmName}")
    private String algorithmName = "md5";
    @Value("${password.hashIterations}")
    private int hashIterations = 2;

    public RandomNumberGenerator getRandomNumberGenerator() {
        return randomNumberGenerator;
    }

    public String getAlgorithmName() {
        return algorithmName;
    }

    public int getHashIterations() {
        return hashIterations;
    }

    public void setRandomNumberGenerator(RandomNumberGenerator randomNumberGenerator) {
        this.randomNumberGenerator = randomNumberGenerator;
    }

    public void setAlgorithmName(String algorithmName) {
        this.algorithmName = algorithmName;
    }

    public void setHashIterations(int hashIterations) {
        this.hashIterations = hashIterations;
    }

    public void encryptPassword(User user) {
        try {
            PsdProcRlt r;
            //对原始密码进行加密处理,处理后得到salt及密文
            r = CryptoApp.PwdProcess(user.getPassword().getBytes());
            user.setSalt(Utils.bytesToHexString(r.getSalt()));//设置salt值
            user.setPassword(Utils.bytesToHexString(r.getValue()));//设置new password
        }catch (Exception e)
        {
            e.printStackTrace();
        }
//        user.setSalt(randomNumberGenerator.nextBytes().toHex());
//
//        String newPassword = new SimpleHash(
//                algorithmName,
//                user.getPassword(),
//                ByteSource.Util.bytes(user.getCredentialsSalt()),
//                hashIterations).toHex();
//
//        user.setPassword(newPassword);

    }
}