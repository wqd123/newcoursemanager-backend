package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class PaperexertypeInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer exercisetype;
    private Integer seq;
    private Integer exampaperid;
    private Integer exercisenum;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getExercisetype() {
        return exercisetype;
    }

    public void setExercisetype(Integer exercisetype) {
        this.exercisetype = exercisetype;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public Integer getExampaperid() {
        return exampaperid;
    }

    public void setExampaperid(Integer exampaperid) {
        this.exampaperid = exampaperid;
    }

    public Integer getExercisenum() {
        return exercisenum;
    }

    public void setExercisenum(Integer exercisenum) {
        this.exercisenum = exercisenum;
    }

    @Override
    public String toString() {
        return "PaperexertypeInfo{" +
        ", id=" + id +
        ", exercisetype=" + exercisetype +
        ", seq=" + seq +
        ", exampaperid=" + exampaperid +
        ", exercisenum=" + exercisenum +
        "}";
    }
}
