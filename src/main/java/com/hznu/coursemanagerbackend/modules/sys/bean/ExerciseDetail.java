package com.hznu.coursemanagerbackend.modules.sys.bean;

import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseInfo;

import java.util.Arrays;

/**
 * @Author: TateBrown
 * @date: 2018/12/10 21:32
 * @param:
 * @return:
 */
public class ExerciseDetail extends ExerciseInfo {
    //关联知识点
    private Integer[] Kpoints;

    private Integer exerciseInfoid;

    private Integer isCollextion;

    public Integer getIsCollextion() {
        return isCollextion;
    }

    public void setIsCollextion(Integer isCollextion) {
        this.isCollextion = isCollextion;
    }

    public Integer getExerciseInfoid() {
        return exerciseInfoid;
    }

    public void setExerciseInfoid(Integer exerciseInfoid) {
        this.exerciseInfoid = exerciseInfoid;
    }

    public Integer[] getKpoints() {
        return Kpoints;
}

    public void setKpoints(Integer[] kpoints) {
        Kpoints = kpoints;
    }

}
