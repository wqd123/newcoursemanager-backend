package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class Exampaper implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private Integer noe;
    private Float totalpoints;
    private Integer dlevel;
    private Integer noet;
    private Integer courseid;
    private Integer teacherid;
    private Integer isshare;

    //附加字段
    @TableField(exist = false)
    private boolean Finish;

    public boolean isFinish() {
        return Finish;
    }

    public void setFinish(boolean finish) {
        Finish = finish;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNoe() {
        return noe;
    }

    public void setNoe(Integer noe) {
        this.noe = noe;
    }

    public Float getTotalpoints() {
        return totalpoints;
    }

    public void setTotalpoints(Float totalpoints) {
        this.totalpoints = totalpoints;
    }

    public Integer getDlevel() {
        return dlevel;
    }

    public void setDlevel(Integer dlevel) {
        this.dlevel = dlevel;
    }

    public Integer getNoet() {
        return noet;
    }

    public void setNoet(Integer noet) {
        this.noet = noet;
    }

    public Integer getCourseid() {
        return courseid;
    }

    public void setCourseid(Integer courseid) {
        this.courseid = courseid;
    }

    public Integer getTeacherid() {
        return teacherid;
    }

    public void setTeacherid(Integer teacherid) {
        this.teacherid = teacherid;
    }

    public Integer getIsshare() {
        return isshare;
    }

    public void setIsshare(Integer isshare) {
        this.isshare = isshare;
    }

    @Override
    public String toString() {
        return "Exampaper{" +
        ", id=" + id +
        ", name=" + name +
        ", noe=" + noe +
        ", totalpoints=" + totalpoints +
        ", dlevel=" + dlevel +
        ", noet=" + noet +
        ", courseid=" + courseid +
        ", teacherid=" + teacherid +
        ", isshare=" + isshare +
        "}";
    }
}
