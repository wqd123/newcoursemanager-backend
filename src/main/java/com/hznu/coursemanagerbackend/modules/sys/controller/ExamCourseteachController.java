package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.hznu.coursemanagerbackend.modules.sys.entity.ExamCourseteach;
import com.hznu.coursemanagerbackend.modules.sys.service.ExamCourseteachService;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 20119-3-26
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/examCourseteach")
@Api(description = "考试班接口",value = "examCourseteach")
public class ExamCourseteachController {

    @Autowired
    ExamCourseteachService examCourseteachService;

    @ApiOperation("返回该考试所有考试班")
    @GetMapping("/courseteachList/{examid}")
    public R courseteachList(@PathVariable Integer examid)
    {
        List<ExamCourseteach> examCourseteaches = examCourseteachService.courseteachList(examid);
        return R.ok().put("list",examCourseteaches);
    }

}
