package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class AnswerRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer studentid;
    private Integer examid;
    private Integer exerciseid;

    //附加字段
    @TableField(exist = false)
    private ExerciseRecord exerciseRecord;
    @TableField(exist = false)
    private ExamRecord examRecord;
    @TableField(exist = false)
    private String answer;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public ExerciseRecord getExerciseRecord() {
        return exerciseRecord;
    }

    public void setExerciseRecord(ExerciseRecord exerciseRecord) {
        this.exerciseRecord = exerciseRecord;
    }

    public ExamRecord getExamRecord() {
        return examRecord;
    }

    public void setExamRecord(ExamRecord examRecord) {
        this.examRecord = examRecord;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudentid() {
        return studentid;
    }

    public void setStudentid(Integer studentid) {
        this.studentid = studentid;
    }

    public Integer getExamid() {
        return examid;
    }

    public void setExamid(Integer examid) {
        this.examid = examid;
    }

    public Integer getExerciseid() {
        return exerciseid;
    }

    public void setExerciseid(Integer exerciseid) {
        this.exerciseid = exerciseid;
    }

    @Override
    public String toString() {
        return "AnswerRecord{" +
        ", id=" + id +
        ", studentid=" + studentid +
        ", examid=" + examid +
        ", exerciseid=" + exerciseid +
        "}";
    }
}
