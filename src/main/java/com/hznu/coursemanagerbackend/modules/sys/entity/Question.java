package com.hznu.coursemanagerbackend.modules.sys.entity;
/**
 * @author WM
 * @since 2018-12-29
 */
public class Question implements Comparable<Question>{


    private int height;//题目高度，有图片时需要使用
    private String content;
    private int type; //题型
    private int grade; //分数

    @Override
    public int compareTo(Question other) {
        return this.type-other.type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
