package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.hznu.coursemanagerbackend.modules.sys.entity.AnswerRecord;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseRecord;
import com.hznu.coursemanagerbackend.modules.sys.dao.ExerciseRecordDao;
import com.hznu.coursemanagerbackend.modules.sys.service.AnswerRecordService;
import com.hznu.coursemanagerbackend.modules.sys.service.ExerciseRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class ExerciseRecordServiceImpl extends ServiceImpl<ExerciseRecordDao, ExerciseRecord> implements ExerciseRecordService {

    @Autowired
    AnswerRecordService answerRecordService;

    @Override
    public void addOne(AnswerRecord answerRecord)
    {
        answerRecordService.insert(answerRecord);
        this.insert(answerRecord.getExerciseRecord());
    }

}
