package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.modules.sys.entity.*;
import com.hznu.coursemanagerbackend.modules.sys.dao.ExamRecordDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExamRecord;
import com.hznu.coursemanagerbackend.modules.sys.service.*;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hznu.coursemanagerbackend.modules.sys.service.ExamRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class ExamRecordServiceImpl extends ServiceImpl<ExamRecordDao, ExamRecord> implements ExamRecordService {

    @Autowired
    ExamRecordService examRecordService;
    @Autowired
    AnswerRecordService answerRecordService;
    @Autowired
    ExerciseInfoService exerciseInfoService;
    @Autowired
    AttendexamService attendexamService;
    @Autowired
    ExamService examService;
    @Autowired
    ExampaperService exampaperService;
    @Autowired
    ComposePaperService composePaperService;
    @Autowired
    ExamCourseteachService examCourseteachService;

    @Override
    public void addRecords(int examid, int studentid, List<ExerciseInfo> exerciseInfos)
    {
        List<AnswerRecord> answerRecordList = answerRecordService.selectList(new EntityWrapper<AnswerRecord>().eq("examid",examid).eq("studentid",studentid));
        for(AnswerRecord answerRecord:answerRecordList)
        {
            examRecordService.delete(new EntityWrapper<ExamRecord>().eq("answerrecordid",answerRecord.getId()));
            answerRecordService.deleteById(answerRecord.getId());
        }
        for(ExerciseInfo exerciseInfo : exerciseInfos)
        {
            AnswerRecord answerRecord = new AnswerRecord();
            answerRecord.setExamid(examid);
            answerRecord.setExerciseid(exerciseInfo.getId());
            answerRecord.setStudentid(studentid);
            answerRecordService.insert(answerRecord);

            ExamRecord examRecord = new ExamRecord();
            examRecord.setAnswer(exerciseInfo.getStudentAnswer());
            examRecord.setAnswerrecordid(answerRecord.getId());


//            int exerType = exerciseInfo.getType();
//            if(exerType == 1||exerType == 2||exerType == 3)
//            {
//                Exam exam = examService.selectById(examid);
//                Exampaper exampaper = exampaperService.selectById(exam.getExampaperid());
//                ComposePaper composePaper = composePaperService.selectOne(new EntityWrapper<ComposePaper>().eq("paperid",exampaper.getId()).eq("exerinfoid",exerciseInfo.getId()));
//                Float points = composePaper.getPoints();
//
//                String answer = exerciseInfoService.selectById(exerciseInfo.getId()).getAnswer();
//                String studentAnswer = exerciseInfo.getStudentAnswer();
//                if(studentAnswer.equals(answer))
//                {
//                    examRecord.setScore(points);
//                }else
//                {
//                    examRecord.setScore(0f);
//                }
//            }
            examRecordService.insert(examRecord);
        }
        Attendexam attendexam = attendexamService.selectOne(new EntityWrapper<Attendexam>().eq("studentid",studentid).eq("examid",examid));
        attendexam.setEndtime(new Date(System.currentTimeMillis()));
        attendexamService.updateById(attendexam);
    }
    @Override
    public List<ExamRecord> studentAnswers(Integer examid,Integer studentid)
    {
        List<ExamRecord> examRecordList = this.baseMapper.selectByExamidAndStudentid(examid,studentid);
        for(ExamRecord examRecord : examRecordList)
        {
            examRecord.setExerciseInfo(exerciseInfoService.selectById(examRecord.getExerciseid()));
        }
        return examRecordList;
    }

    @Override
    public void modifyScores(List<ExamRecord>examRecords)
    {
        examRecordService.updateBatchById(examRecords);
    }

    @Override
    public void markByExamCourseteachid(Integer examCourseteachid)
    {
        ExamCourseteach examCourseteach = examCourseteachService.selectById(examCourseteachid);
        int examid = examCourseteach.getExamid();
        int courseteachid = examCourseteach.getCourseteachid();
        List<Attendexam> attendexamList = attendexamService.findStudentByExamidAndCourseteachid(examid,courseteachid);
        for(Attendexam attendexam : attendexamList)
        {
            System.out.println(attendexam);
            List<AnswerRecord> answerRecordList = answerRecordService.selectList(new EntityWrapper<AnswerRecord>().eq("studentid",attendexam.getStudentid()).eq("examid",attendexam.getExamid()));
            for(AnswerRecord answerRecord : answerRecordList)
            {
                ExerciseInfo exerciseInfo = exerciseInfoService.selectById(answerRecord.getExerciseid());
                int exerType = exerciseInfo.getType();
                if(exerType == 1||exerType == 2||exerType == 3)
                {
                    ExamRecord examRecord = examRecordService.selectOne(new EntityWrapper<ExamRecord>().eq("answerrecordid", answerRecord.getId()));
                    String answer = exerciseInfo.getAnswer();
                    String studentanswer = examRecord.getAnswer();
                    if(studentanswer.equals(answer))
                    {
                        Exam exam = examService.selectById(answerRecord.getExamid());
                        Exampaper exampaper = exampaperService.selectById(exam.getExampaperid());
                        ComposePaper composePaper = composePaperService.selectOne(new EntityWrapper<ComposePaper>().eq("paperid",exampaper.getId()).eq("exerinfoid",exerciseInfo.getId()));
                        float points = composePaper.getPoints();
                        examRecord.setScore(points);
                    }else
                    {
                        examRecord.setScore(0f);
                    }
                    examRecordService.updateById(examRecord);
                }

            }
        }
    }
}
