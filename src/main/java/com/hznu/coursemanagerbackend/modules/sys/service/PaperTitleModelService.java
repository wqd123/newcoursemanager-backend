package com.hznu.coursemanagerbackend.modules.sys.service;


import com.baomidou.mybatisplus.service.IService;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.entity.PaperTitleModel;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weimeng
 * @since 2019-04-02
 */
public interface PaperTitleModelService extends IService<PaperTitleModel> {
    @Transactional
    PageUtils queryPage(Map<String,Object> params);
}
