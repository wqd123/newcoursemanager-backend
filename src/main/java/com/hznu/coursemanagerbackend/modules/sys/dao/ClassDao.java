package com.hznu.coursemanagerbackend.modules.sys.dao;

import com.hznu.coursemanagerbackend.modules.sys.entity.Class;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface ClassDao extends BaseMapper<Class> {

    List<Class> selectBykeyword(@Param("keyword") String keyword);

}
