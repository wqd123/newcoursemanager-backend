package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.hznu.coursemanagerbackend.myBeans.TreeSupport;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class Class extends TreeSupport implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private Integer majorid;
    private Integer departmentid;
    private Integer schoolid;
    private Integer universityid;

    /**
     * 附加字段，构造树
     * @return
     */
    @TableField(exist = false)
    private final int adminrank = 5;
    @TableField(exist = false)
    private Major major;
    @TableField(exist = false)
    private Department department;
    @TableField(exist = false)
    private School school;

    public Major getMajor() {
        return major;
    }

    public void setMajor(Major major) {
        this.major = major;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMajorid() {
        return majorid;
    }

    public void setMajorid(Integer majorid) {
        this.majorid = majorid;
    }

    public Integer getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(Integer departmentid) {
        this.departmentid = departmentid;
    }

    public Integer getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Integer schoolid) {
        this.schoolid = schoolid;
    }

    public Integer getUniversityid() {
        return universityid;
    }

    public void setUniversityid(Integer universityid) {
        this.universityid = universityid;
    }

    public int getAdminrank() {
        return adminrank;
    }

    @Override
    public String toString() {
        return "Class{" +
        ", id=" + id +
        ", name=" + name +
        ", majorid=" + majorid +
        ", departmentid=" + departmentid +
        ", schoolid=" + schoolid +
        ", universityid=" + universityid +
        "}";
    }
}
