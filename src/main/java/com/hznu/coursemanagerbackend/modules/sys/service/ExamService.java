package com.hznu.coursemanagerbackend.modules.sys.service;

import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.entity.Exam;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface ExamService extends IService<Exam> {

//    PageUtils queryPageByCourseteachid(Map<String,Object> params);

    PageUtils queryPageByCourseteachid(Map<String,Object> params);

    void add(Exam exam);

}
