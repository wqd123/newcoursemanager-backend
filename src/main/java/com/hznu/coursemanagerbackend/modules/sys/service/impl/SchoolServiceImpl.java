package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.common.utils.BeanUtils;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.entity.School;
import com.hznu.coursemanagerbackend.modules.sys.dao.SchoolDao;
import com.hznu.coursemanagerbackend.modules.sys.service.SchoolService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class SchoolServiceImpl extends ServiceImpl<SchoolDao, School> implements SchoolService {

    @Override
    public School findById(Integer schoolid) {
        School school = this.selectById(schoolid);
        return school;
    }

    @Override
    public PageUtils queryPage(Map<String,Object> params)
    {
        String keyword = (String)params.get("keyword");
        Page<School> page = this.selectPage(
                new Query<School>(params).getPage(),
                new EntityWrapper<School>().allEq(BeanUtils.convertBeanToMap(new School(),params)).andNew(keyword!=null,null).
                        like(keyword!=null,"name",keyword)
        );
        return new PageUtils(page);
    }

}
