package com.hznu.coursemanagerbackend.modules.sys.service;

import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.entity.Major;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface MajorService extends IService<Major> {

    List<Major> findByName(Major major);

    Major findById(Integer id);

    PageUtils queryPage(Map<String,Object> params);

    PageUtils queryPageByDepartmentId(Map<String,Object> params);

}
