package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class CourseStudent implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer courseteachid;
    private Integer studentid;

    //附加字段
    @TableField(exist = false)
    private StudentInfo studentInfo;
    @TableField(exist = false)
    CourseTeach courseTeach;
    @TableField(exist = false)
    private User user;

    public CourseTeach getCourseTeach() {
        return courseTeach;
    }

    public void setCourseTeach(CourseTeach courseTeach) {
        this.courseTeach = courseTeach;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public StudentInfo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(StudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCourseteachid() {
        return courseteachid;
    }

    public void setCourseteachid(Integer courseteacherid) {
        this.courseteachid = courseteacherid;
    }

    public Integer getStudentid() {
        return studentid;
    }

    public void setStudentid(Integer studentid) {
        this.studentid = studentid;
    }

    @Override
    public String toString() {
        return "CourseStudent{" +
        ", id=" + id +
        ", courseteachid=" + courseteachid +
        ", studentid=" + studentid +
        "}";
    }
}
