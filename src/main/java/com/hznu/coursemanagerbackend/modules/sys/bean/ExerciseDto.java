package com.hznu.coursemanagerbackend.modules.sys.bean;

public class ExerciseDto {
    private Integer studentId;

    private Integer flag;

    //关联知识点
    private Integer[] Kpoints;

    private Integer dlevel;

    private Integer type;

    private Integer courseId;

    private Integer isready;

    public Integer getIsready() {
        return isready;
    }

    public void setIsready(Integer isready) {
        this.isready = isready;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer[] getKpoints() {
        return Kpoints;
    }

    public void setKpoints(Integer[] kpoints) {
        Kpoints = kpoints;
    }

    public Integer getDlevel() {
        return dlevel;
    }

    public void setDlevel(Integer dlevel) {
        this.dlevel = dlevel;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }
}
