package com.hznu.coursemanagerbackend.modules.sys.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseCollection;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseInfo;
import com.hznu.coursemanagerbackend.modules.sys.service.ExerciseCollectionService;
import com.hznu.coursemanagerbackend.modules.sys.service.ExerciseInfoService;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Wqd
 * @since 2019-03-01
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/exerciseCollection")
@Api(description = "题目收藏表",value = "exerciseCollection")
public class ExerciseCollectionController {

    @Autowired
    ExerciseCollectionService exerciseCollectionService;

    @Autowired
    ExerciseInfoService exerciseInfoService;

    /**
     * @author: Wqd
     * @Date: 2019/3/2
     * @Description:
     */
    @ApiOperation("收藏题目")
    @PostMapping("/add")
    public R add(@RequestBody ExerciseCollection exerciseCollection) {
        exerciseCollectionService.insert(exerciseCollection);
        return R.ok("收藏成功");
    }


    @ApiOperation("根据学生id和课程id查找收藏的题目")
    @PostMapping("/findbyStudentidAndCourseid/{studentid}/{courseid}")
    public R findbyStudentidAndCourseid(@PathVariable Integer studentid,@PathVariable Integer courseid) {
        List<ExerciseCollection> exerciseCollectionList = exerciseCollectionService.selectList(new EntityWrapper<ExerciseCollection>().eq("studentid",studentid).eq("courseid",courseid));
        for(ExerciseCollection exerciseCollection:exerciseCollectionList){
            ExerciseInfo exerciseInfo = exerciseInfoService.selectById(exerciseCollection.getExerciseinfoid());
            exerciseCollection.setExerciseInfo(exerciseInfo);
        }
        return R.ok().put("data", exerciseCollectionList);
    }

    /**
     * @author: Wqd
     * @Date: 2019/4/1
     * @Description:
     */
    @ApiOperation("删除收藏的题目")
    @GetMapping("/remove/{id}")
    public R remove(@PathVariable Integer id) {
        exerciseCollectionService.deleteById(id);
        return R.ok("删除成功");
    }

}

