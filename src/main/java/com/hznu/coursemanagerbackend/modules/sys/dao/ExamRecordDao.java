package com.hznu.coursemanagerbackend.modules.sys.dao;

import com.hznu.coursemanagerbackend.modules.sys.entity.ExamRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface ExamRecordDao extends BaseMapper<ExamRecord> {

    List<ExamRecord> selectByExamidAndStudentid(@Param("examid") Integer examid, @Param("studentid") Integer studentid);

}
