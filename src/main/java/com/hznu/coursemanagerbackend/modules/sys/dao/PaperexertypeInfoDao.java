package com.hznu.coursemanagerbackend.modules.sys.dao;

import com.hznu.coursemanagerbackend.modules.sys.entity.PaperexertypeInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface PaperexertypeInfoDao extends BaseMapper<PaperexertypeInfo> {

}
