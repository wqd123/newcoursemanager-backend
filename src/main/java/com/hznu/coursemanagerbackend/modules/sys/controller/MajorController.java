package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.validator.ValidatorUtils;
import com.hznu.coursemanagerbackend.common.validator.group.AddGroup;
import com.hznu.coursemanagerbackend.modules.sys.entity.Class;
import com.hznu.coursemanagerbackend.modules.sys.entity.Department;
import com.hznu.coursemanagerbackend.modules.sys.entity.Major;
import com.hznu.coursemanagerbackend.modules.sys.service.ClassService;
import com.hznu.coursemanagerbackend.modules.sys.service.DepartmentService;
import com.hznu.coursemanagerbackend.modules.sys.service.MajorService;
import com.hznu.coursemanagerbackend.modules.sys.service.impl.ClassServiceImpl;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/major")
@Api(description = "专业接口",value = "major")
public class MajorController {

    @Autowired
    MajorService majorService;

    @Autowired
    DepartmentService departmentService;

    @Autowired
    ClassService classService;

    /**
     * @author Wqd
     * @since 2018-11-28
     */
    @ApiOperation("根据系部id查找专业")
    @PostMapping("/findmajorbydepid/{departmentid}")
    public R findmajorbydepid(@PathVariable Integer departmentid) {
        try {
            List<Major> list = majorService.selectList(new EntityWrapper<Major>().eq("departmentid",departmentid));
            return R.ok().put("data", list);
        } catch (Exception e) {
            e.printStackTrace();
            return R.error();
        }
    }

    @ApiOperation("添加专业")
    @PostMapping("/add")
    public R add(@RequestBody Major major){
        try{
            if(majorService.findByName(major).size()!=0){
                return R.error("该专业或类似名称专业已存在");
            }else {
                Department department = departmentService.findById(major.getDepartmentid());
                if(department!=null){
                    major.setSchoolid(department.getSchoolid());
                    major.setUniversityid(department.getUniversityid());
                    majorService.insert(major);
                }else{
                    return R.error("请先创建专业");
                }

            }
            return R.ok();
        }catch (Exception e){
            e.printStackTrace();
            return R.error();
        }
    }

    /**
     * @author Wqd
     * @since 2019-3-28
     */
    @ApiOperation("删除专业")
    @GetMapping("/removemajor/{id}")
    public R removemajor(@PathVariable Integer id) {
        try {
            if(classService.selectList(new EntityWrapper<Class>().eq("majorid",id)).size()==0){
                return R.ok("删除成功").put("data", majorService.deleteById(id));
            }else{
                return R.error("专业下有班级，不能删除");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("程序异常");
        }
    }

    /**
     * @author Wqd
     * @since 2019-3-31
     */
    @ApiOperation("条件查询专业（分页）")
    @PostMapping("/list")
    public R list(@RequestParam Map<String,Object> params)
    {
        PageUtils page = majorService.queryPage(params);
        return R.ok().put("page",page);
    }

    @ApiOperation("根据系部id筛选专业（分页）")
    @PostMapping("/findMajorByDepartmentId")
    public R findMajorByDepartmentId(@RequestParam Map<String,Object> params)
    {
        PageUtils page = majorService.queryPageByDepartmentId(params);
        return R.ok().put("page",page);
    }

    @ApiOperation("修改专业信息")
    @PostMapping("/modify")
    public R modify(@RequestBody Major major) {
        ValidatorUtils.validateEntity(major, AddGroup.class);
        majorService.updateById(major);
        return R.ok();
    }
}

