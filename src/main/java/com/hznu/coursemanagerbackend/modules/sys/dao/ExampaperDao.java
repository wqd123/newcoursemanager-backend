package com.hznu.coursemanagerbackend.modules.sys.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.modules.sys.entity.Exam;
import com.hznu.coursemanagerbackend.modules.sys.entity.Exampaper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface ExampaperDao extends BaseMapper<Exampaper> {
}
