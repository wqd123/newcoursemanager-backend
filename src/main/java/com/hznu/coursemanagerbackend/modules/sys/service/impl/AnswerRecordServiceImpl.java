package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.hznu.coursemanagerbackend.modules.sys.entity.AnswerRecord;
import com.hznu.coursemanagerbackend.modules.sys.dao.AnswerRecordDao;
import com.hznu.coursemanagerbackend.modules.sys.service.AnswerRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class AnswerRecordServiceImpl extends ServiceImpl<AnswerRecordDao, AnswerRecord> implements AnswerRecordService {

}
