package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.alibaba.druid.sql.PagerUtils;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.entity.Course;
import com.hznu.coursemanagerbackend.modules.sys.dao.CourseDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.TeacherInfo;
import com.hznu.coursemanagerbackend.modules.sys.service.CourseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hznu.coursemanagerbackend.modules.sys.service.TeacherInfoService;
import com.hznu.coursemanagerbackend.modules.sys.service.UserService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseDao, Course> implements CourseService {

    @Autowired
    UserService userService;

    @Autowired
    TeacherInfoService teacherInfoService;

    @Override
    public PageUtils queryPage(Map<String,Object> params)
    {
        String keyword=(String)params.get("keyword");
        Page<Course> page = this.selectPage(
                new Query<Course>(params).getPage(),
                new EntityWrapper<Course>().like(keyword!=null,"name",keyword)
                );
        List<Course> courseList=page.getRecords();
        for(Course course:courseList){
            course.setCreator(userService.selectById(teacherInfoService.selectById(course.getCreatorid()).getUserid()).getRealname());
            course.setMaintainer(userService.selectById(teacherInfoService.selectById(course.getMaintainerid()).getUserid()).getRealname());
        }
        page.setRecords(courseList);
        return new PageUtils(page);
    }

    @Override
    public Course findById(Integer id)
    {
        Course course = this.selectById(id);
        course.setCreator(userService.selectById(teacherInfoService.selectById(course.getCreatorid()).getUserid()).getRealname());
        course.setMaintainer(userService.selectById(teacherInfoService.selectById(course.getMaintainerid()).getUserid()).getRealname());
        return course;
    }

}
