package com.hznu.coursemanagerbackend.modules.sys.service;

import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.bean.UserLoadBase;
import com.hznu.coursemanagerbackend.modules.sys.entity.User;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface UserService extends IService<User> {

    void add(User user);

    User findById(Integer id);

    PageUtils queryPage(Map<String,Object> params);

    void modify(User user);

    Boolean validatePassword(User sessionUser, String password);
    Integer LoadBatch(List<Object> objectList, UserLoadBase userLoadBase);
}
