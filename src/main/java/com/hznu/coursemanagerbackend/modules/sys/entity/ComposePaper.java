package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class ComposePaper implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer paperid;
    private Integer exerinfoid;
    private Float points;
    private Integer seq;

    @TableField(exist = false)
    ExerciseInfo exerciseInfo;

    public ExerciseInfo getExerciseInfo() {
        return exerciseInfo;
    }

    public void setExerciseInfo(ExerciseInfo exerciseInfo) {
        this.exerciseInfo = exerciseInfo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPaperid() {
        return paperid;
    }

    public void setPaperid(Integer paperid) {
        this.paperid = paperid;
    }

    public Integer getExerinfoid() {
        return exerinfoid;
    }

    public void setExerinfoid(Integer exerinfoid) {
        this.exerinfoid = exerinfoid;
    }

    public Float getPoints() {
        return points;
    }

    public void setPoints(Float points) {
        this.points = points;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    @Override
    public String toString() {
        return "ComposePaper{" +
        ", id=" + id +
        ", paperid=" + paperid +
        ", exerinfoid=" + exerinfoid +
        ", points=" + points +
        ", seq=" + seq +
        "}";
    }
}
