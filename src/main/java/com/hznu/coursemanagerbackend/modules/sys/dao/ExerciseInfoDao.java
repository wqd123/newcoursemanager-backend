package com.hznu.coursemanagerbackend.modules.sys.dao;

import com.hznu.coursemanagerbackend.modules.sys.bean.ExerciseDetail;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Component
public interface ExerciseInfoDao extends BaseMapper<ExerciseInfo> {
     List<ExerciseInfo> selectByCondition(ExerciseDetail exerciseDetail);
}
