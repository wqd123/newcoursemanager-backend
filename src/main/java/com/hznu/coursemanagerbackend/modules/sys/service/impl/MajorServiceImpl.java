package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.common.utils.BeanUtils;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.entity.Department;
import com.hznu.coursemanagerbackend.modules.sys.entity.Major;
import com.hznu.coursemanagerbackend.modules.sys.dao.MajorDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.School;
import com.hznu.coursemanagerbackend.modules.sys.service.DepartmentService;
import com.hznu.coursemanagerbackend.modules.sys.service.MajorService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hznu.coursemanagerbackend.modules.sys.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hznu.coursemanagerbackend.modules.sys.entity.Major;
import com.hznu.coursemanagerbackend.modules.sys.service.base.BaseService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class MajorServiceImpl extends ServiceImpl<MajorDao, Major> implements MajorService {

    @Autowired
    MajorDao majorDao;

    @Autowired
    DepartmentService departmentService;

    @Autowired
    MajorService majorService;

    @Autowired
    SchoolService schoolService;

    @Override
    public List<Major> findByName(Major major) {
        List<Major> major1 = this.selectList(new EntityWrapper<Major>().eq("name",major.getName()));
        return major1;
    }

    @Override
    public Major findById(Integer majorid) {
        Major major = this.selectById(majorid);
        return major;
    }

    @Override
    public PageUtils queryPage(Map<String,Object> params)
    {
        String keyword = (String)params.get("keyword");
        Page<Major> page = this.selectPage(
                new Query<Major>(params).getPage(),
                new EntityWrapper<Major>().allEq(BeanUtils.convertBeanToMap(new Major(),params)).andNew(keyword!=null,null).
                        like(keyword!=null,"name",keyword)
        );
        for(Major major : page.getRecords())
        {
            Department department = departmentService.selectById(major.getDepartmentid());
            major.setDepartment(department);
            School school = schoolService.selectById(major.getSchoolid());
            major.setSchool(school);
        }
        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageByDepartmentId(Map<String,Object> params)
    {
        Page<Major> page = this.selectPage(
                new Query<Major>(params).getPage(),
                new EntityWrapper<Major>().allEq(BeanUtils.convertBeanToMap(new Major(),params))
        );
        for(Major major : page.getRecords()){
            Major major1 = majorService.selectOne(new EntityWrapper<Major>().eq("departmentid",major.getDepartmentid()));
            major.setName(major1.getName());
            major.setSchoolid(major1.getSchoolid());
            major.setUniversityid(major1.getUniversityid());
            School school = schoolService.selectById(major.getSchoolid());
            major.setSchool(school);
            Department department = departmentService.selectById(major.getDepartmentid());
            major.setDepartment(department);
        }
        return new PageUtils(page);
    }

}
