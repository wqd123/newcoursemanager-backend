package com.hznu.coursemanagerbackend.modules.sys.service;

import com.hznu.coursemanagerbackend.modules.sys.entity.Question;

import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class SaveQuestionService {
    public List<Question> questions = new LinkedList<>();
    public void add(Question question){
        questions.add(question);
    }
}
