package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.hznu.coursemanagerbackend.modules.sys.dao.ExerciseCollectionDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseCollection;
import com.hznu.coursemanagerbackend.modules.sys.service.ExerciseCollectionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2019-03-01
 */
@Service
public class ExerciseCollectionServiceImpl extends ServiceImpl<ExerciseCollectionDao, ExerciseCollection> implements ExerciseCollectionService {

}
