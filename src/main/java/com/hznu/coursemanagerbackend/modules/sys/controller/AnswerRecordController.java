package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.modules.sys.entity.AnswerRecord;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseRecord;
import com.hznu.coursemanagerbackend.modules.sys.service.AnswerRecordService;
import com.hznu.coursemanagerbackend.modules.sys.service.ExerciseRecordService;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/answerRecord")
@Api(description = "答案记录表",value = "answerRecord")
public class AnswerRecordController {

    @Autowired
    AnswerRecordService answerRecordService;

    @Autowired
    ExerciseRecordService exerciseRecordService;

    /**
     * @Description:保存练习答案
     * @param:
     * @return:
     * @author: Wqd
     * @Date: 2019/3/10
     */
    @ApiOperation("保存答案")
    @PostMapping("/Save")
    public R Save(@RequestBody AnswerRecord answerRecord){
        AnswerRecord answerRecord1 = new AnswerRecord();
        answerRecord1.setExamid(-1);
        answerRecord1.setExerciseid(answerRecord.getExerciseid());
        answerRecord1.setStudentid(answerRecord.getStudentid());
        answerRecordService.insert(answerRecord1);
        ExerciseRecord exerciseRecord = new ExerciseRecord();
        exerciseRecord.setAnswer(answerRecord.getAnswer());
        exerciseRecord.setAnswerrecordid(answerRecord1.getId());
        exerciseRecordService.insert(exerciseRecord);
        return R.ok("保存成功！");
    }

}

