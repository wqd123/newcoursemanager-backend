package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.hznu.coursemanagerbackend.modules.sys.entity.ExamRecord;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseInfo;
import com.hznu.coursemanagerbackend.modules.sys.service.ExamRecordService;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/examRecord")
@Api(description = "考试答案记录接口",value = "examRecord")
public class ExamRecordController {

    @Autowired
    ExamRecordService examRecordService;

    @PostMapping("/addRecords/{examid}/{studentid}")
    @ApiOperation("交卷答案保存")
    public R addRecords(@PathVariable Integer examid , @PathVariable Integer studentid, @RequestBody List<ExerciseInfo> exerciseInfos)
    {
        try
        {
            examRecordService.addRecords(examid,studentid,exerciseInfos);
            return R.ok();
        }catch (Exception e)
        {
            e.printStackTrace();
            return R.error();
        }
    }

    @PostMapping("/markByExamCourseteachid/{examCourseteachid}")
    @ApiOperation("/批改某个考试班的所有答卷")
    public R markByExamCourseteachid(@PathVariable Integer examCourseteachid)
    {
        examRecordService.markByExamCourseteachid(examCourseteachid);
        return R.ok();
    }

    @GetMapping("/studentAnswers/{examid}/{studentid}")
    @ApiOperation("返回某个学生该场考试的所有答题情况")
    public R studentAnswers(@PathVariable Integer examid, @PathVariable Integer studentid)
    {
        List<ExamRecord> examRecordList = examRecordService.studentAnswers(examid,studentid);
        return R.ok().put("list",examRecordList);
    }

    @PostMapping("/subScore")
    @ApiOperation("提交批改分数")
    public R subScore(@RequestBody List<ExamRecord> examRecords)
    {
        try
        {
            examRecordService.modifyScores(examRecords);
            return R.ok();
        }catch (Exception e)
        {
            e.printStackTrace();
            return R.error();
        }
    }
}

