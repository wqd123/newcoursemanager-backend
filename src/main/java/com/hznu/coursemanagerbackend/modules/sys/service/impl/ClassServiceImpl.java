package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.common.utils.BeanUtils;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.entity.Class;
import com.hznu.coursemanagerbackend.modules.sys.dao.ClassDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.Department;
import com.hznu.coursemanagerbackend.modules.sys.entity.Major;
import com.hznu.coursemanagerbackend.modules.sys.entity.School;
import com.hznu.coursemanagerbackend.modules.sys.service.ClassService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hznu.coursemanagerbackend.modules.sys.service.DepartmentService;
import com.hznu.coursemanagerbackend.modules.sys.service.MajorService;
import com.hznu.coursemanagerbackend.modules.sys.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class ClassServiceImpl extends ServiceImpl<ClassDao, Class> implements ClassService {

    @Autowired
    DepartmentService departmentService;

    @Autowired
    MajorService majorService;

    @Autowired
    SchoolService schoolService;

    @Autowired
    ClassService classService;

    @Override
    public List<Class> findByName(Class clazz) {
        List<Class> clazz1 = this.selectList(new EntityWrapper<Class>().eq("name",clazz.getName()));
        return clazz1;
    }

    @Override
    public List<Class> selectBykeyword(String name) {
        return this.baseMapper.selectBykeyword(name);
    }

    @Override
    public PageUtils queryPage(Map<String,Object> params)
    {
        String keyword = (String)params.get("keyword");
        Page<Class> page = this.selectPage(
                new Query<Class>(params).getPage(),
                new EntityWrapper<Class>().allEq(BeanUtils.convertBeanToMap(new Class(),params)).andNew(keyword!=null,null).
                        like(keyword!=null,"name",keyword)
        );
        for(Class clazz : page.getRecords())
        {
            Major major = majorService.selectById(clazz.getMajorid());
            clazz.setMajor(major);
            Department department = departmentService.selectById(clazz.getDepartmentid());
            clazz.setDepartment(department);
            School school = schoolService.selectById(clazz.getSchoolid());
            clazz.setSchool(school);
        }
        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageByMajorId(Map<String,Object> params)
    {
        Integer majorid = Integer.parseInt((String)params.get("majorid"));
        Page<Class> page = new Query<Class>(params).getPage();
        List<Class> classList = classService.selectList(new EntityWrapper<Class>().eq("majorid",majorid));
        for(Class clazz : classList)
        {
            Major major = majorService.selectById(clazz.getMajorid());
            clazz.setMajor(major);
            Department department = departmentService.selectById(clazz.getDepartmentid());
            clazz.setDepartment(department);
            School school = schoolService.selectById(clazz.getSchoolid());
            clazz.setSchool(school);
        }
        page.setRecords(classList);
        return new PageUtils(page);
    }

}
