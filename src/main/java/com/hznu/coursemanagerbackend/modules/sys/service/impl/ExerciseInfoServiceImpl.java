package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.common.utils.MapUtils;
import com.hznu.coursemanagerbackend.common.utils.Utils;
import com.hznu.coursemanagerbackend.common.validator.Assert;
import com.hznu.coursemanagerbackend.modules.sys.bean.ExerciseDetail;
import com.hznu.coursemanagerbackend.modules.sys.entity.AnswerRecord;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseInfo;
import com.hznu.coursemanagerbackend.modules.sys.dao.ExerciseInfoDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerkPoint;
import com.hznu.coursemanagerbackend.modules.sys.entity.Kpoint;
import com.hznu.coursemanagerbackend.modules.sys.service.AnswerRecordService;
import com.hznu.coursemanagerbackend.modules.sys.service.ExerciseInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hznu.coursemanagerbackend.modules.sys.service.ExerkPointService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class ExerciseInfoServiceImpl extends ServiceImpl<ExerciseInfoDao, ExerciseInfo> implements ExerciseInfoService {

    @Autowired
    ExerkPointService exerkPointService;

    @Autowired
    AnswerRecordService answerRecordService;

    @Autowired
    private ExerciseInfoDao exerciseInfoDao;

    @Override
    public Integer LoadBatch(List<Object> objectList, Integer courseId) {
        Integer count = 0;
        try {
            for (Object object : objectList) {
                Map<String, Object> map = MapUtils.getKeyAndValue(object);
                if (map.get("exerciseType") == null || map.get("exerciseContent") == null) {
                    continue;
                }
                Integer type = Integer.parseInt((String) map.get("exerciseType"));//题型
                Integer dlevel;
                if ((String) map.get("dlevel") == null) {
                    dlevel = 0;
                }
                dlevel = Integer.parseInt((String) map.get("dlevel"));//难度
                String content = Utils.AddTag((String) map.get("exerciseContent"), "p");
                String comment = Utils.AddTag((String) map.get("comment"), "p");
                String answer = "";
                //根据题型拼接题干答案
                switch (type) {
                    case 1:
                    case 2:
                        if (map.get("choosNum") != null) {
                            Integer choose = Integer.parseInt((String) map.get("choosNum"));
                            if (choose > 0 && choose <= 8)
                                for (char i = 'A'; i < 'A' + choose; i++) {
                                    String curChoose = Utils.AddTag((String) map.get("choose" + i), "p");
                                    content = content + curChoose;
                                }
                        }
                        if (map.get("answer") != null) {
                            answer = (String) map.get("answer");
                        }
                    case 3:
                        if (map.get("answer") != null) {
                            answer = (String) map.get("answer");
                        }
                        break;
                    case 4:
                        if (map.get("answer") != null) {
                            answer = Utils.AddTag((String) map.get("answer"), "p");
                        }
                        break;
                    default:
                        break;
                }
                ExerciseInfo exerciseInfo = new ExerciseInfo();
                exerciseInfo.setAnswer(answer);
                exerciseInfo.setComment(comment);
                exerciseInfo.setCourseid(courseId);
                exerciseInfo.setContent(content);
                exerciseInfo.setDlevel(dlevel);
                exerciseInfo.setType(type);
                this.insert(exerciseInfo);
                count++;//添加成功计数
            }
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return count;
        }
    }

    @Override
    public List<ExerciseDetail> findByCondition(ExerciseDetail exerciseDetail) {
        List<ExerciseInfo> exerciseInfoList = exerciseInfoDao.selectByCondition(exerciseDetail);
        List<ExerciseDetail> exerciseDetailList = new LinkedList<ExerciseDetail>();
        for (ExerciseInfo exerciseInfo : exerciseInfoList) {
            ExerciseDetail exerciseDetail1 = new ExerciseDetail();
            List<ExerkPoint> exerkPointList = exerkPointService.selectList(new EntityWrapper<ExerkPoint>().eq("exerinfoid", exerciseInfo.getId()));
            exerciseDetail1.setAnswer(exerciseInfo.getAnswer());
            exerciseDetail1.setComment(exerciseInfo.getComment());
            exerciseDetail1.setContent(exerciseInfo.getContent());
            exerciseDetail1.setCourseid(exerciseInfo.getCourseid());
            exerciseDetail1.setDlevel(exerciseInfo.getDlevel());
            exerciseDetail1.setId(exerciseInfo.getId());
            exerciseDetail1.setType(exerciseInfo.getType());
            exerciseDetail1.setIsready(exerciseInfo.getIsready());
            exerciseDetail1.setIsremove(exerciseInfo.getIsremove());
            if (exerkPointList.size() != 0) {
                List<Integer> tmp = new LinkedList<Integer>();
                for (ExerkPoint exerkPoint : exerkPointList) {
                    tmp.add(exerkPoint.getKpointid());
                }
                Integer[] tmpArray = new Integer[tmp.size()];
                exerciseDetail1.setKpoints(tmp.toArray(tmpArray));
            } else {
                exerciseDetail1.setKpoints(null);
            }
            exerciseDetailList.add(exerciseDetail1);
        }
        return exerciseDetailList;
    }

}
