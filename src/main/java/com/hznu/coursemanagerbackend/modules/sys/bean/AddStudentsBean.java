package com.hznu.coursemanagerbackend.modules.sys.bean;

public class AddStudentsBean {
    private Integer courseteachid;

    private String[] workidList;

    public Integer getCourseteachid() {
        return courseteachid;
    }

    public void setCourseteachid(Integer courseteachid) {
        this.courseteachid = courseteachid;
    }

    public String[] getWorkidList() {
        return workidList;
    }

    public void setWorkidList(String[] workidList) {
        this.workidList = workidList;
    }
}
