package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.common.utils.BeanUtils;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.entity.ComposePaper;
import com.hznu.coursemanagerbackend.modules.sys.entity.CourseStudent;
import com.hznu.coursemanagerbackend.modules.sys.entity.CourseTeach;
import com.hznu.coursemanagerbackend.modules.sys.entity.Exampaper;
import com.hznu.coursemanagerbackend.modules.sys.dao.ExampaperDao;
import com.hznu.coursemanagerbackend.modules.sys.service.ComposePaperService;
import com.hznu.coursemanagerbackend.modules.sys.service.ExampaperService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class ExampaperServiceImpl extends ServiceImpl<ExampaperDao, Exampaper> implements ExampaperService {

    @Autowired
    ExampaperService exampaperService;
    @Autowired
    ComposePaperService composePaperService;

    @Override
    public PageUtils queryPage(Map<String,Object> params)
    {
        String keyword = (String)params.get("keyword");

        Page<Exampaper> page = this.selectPage(
                new Query<Exampaper>(params).getPage(),
                new EntityWrapper<Exampaper>().allEq(BeanUtils.convertBeanToMap(new Exampaper(),params))
        );
        return new PageUtils(page);
    }

    @Override
    public PageUtils findByCourse(Map<String,Object> params)
    {
        Integer courseid = Integer.parseInt((String)params.get("courseid"));
        Page<Exampaper> page = new Query<Exampaper>(params).getPage();
        List<Exampaper> exampapers = exampaperService.selectList(new EntityWrapper<Exampaper>().eq("courseid",courseid));
        for(Exampaper exampaper : exampapers)
        {
            float totpoints = 0;
            List<ComposePaper> composePapers = composePaperService.selectList(new EntityWrapper<ComposePaper>().eq("paperid",exampaper.getId()));
            for(ComposePaper composePaper : composePapers)
            {
                if(composePaper.getPoints()==null){
                    totpoints+=0;
                }else{
                    totpoints += composePaper.getPoints();
                }
            }
            if(Math.abs(totpoints-exampaper.getTotalpoints())<0.001)
            {
                exampaper.setFinish(true);
            }else
            {
                exampaper.setFinish(false);
            }
        }
        page.setRecords(exampapers);
        return new PageUtils(page);
    }
}
