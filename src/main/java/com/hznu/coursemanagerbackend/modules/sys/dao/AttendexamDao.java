package com.hznu.coursemanagerbackend.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hznu.coursemanagerbackend.modules.sys.entity.Attendexam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface AttendexamDao extends BaseMapper<Attendexam> {

    List<Attendexam> selectByExamidAndCourseteachid(@Param("examid")Integer examid,@Param("courseteachid")Integer courseteachid);

}
