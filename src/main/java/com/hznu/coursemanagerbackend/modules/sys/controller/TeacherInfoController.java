package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.modules.sys.entity.Course;
import com.hznu.coursemanagerbackend.modules.sys.entity.CourseTeach;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseInfo;
import com.hznu.coursemanagerbackend.modules.sys.entity.TeacherInfo;
import com.hznu.coursemanagerbackend.modules.sys.service.*;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/teacherInfo")
public class TeacherInfoController {

    @Autowired
    TeacherInfoService teacherInfoService;

    @Autowired
    CourseService courseService;

    @Autowired
    CourseTeachService courseTeachService;

    @Autowired
    UserService userService;

    @Autowired
    ExerciseInfoService exerciseInfoService;

    /**
     * @author: DragonDoor
     * @Date: 2018/12/19 13:29
     * @Description:
     */
    @ApiOperation("通过用户id查找教师信息")
    @GetMapping("/findTeacherByUserid/{userid}")
    public R findTeacherByUserid(@PathVariable Integer userid) {
        try {
            TeacherInfo teacherInfo = teacherInfoService.selectOne(new EntityWrapper<TeacherInfo>().eq("userid",userid));
            return R.ok().put("teacherInfo", teacherInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return R.error(e.getMessage());
        }
    }

    /**
     * @author Wqd
     * @since 2019-3-28
     */
    @ApiOperation("删除教师")
    @GetMapping("/removeteacher/{id}")
    public R removeteacher(@PathVariable Integer userid) {
        try {
            TeacherInfo teacherInfo = teacherInfoService.selectOne(new EntityWrapper<TeacherInfo>().eq("userid",userid));
            if(courseService.selectCount(new EntityWrapper<Course>().eq("creatorid",teacherInfo.getId()))==0&&
                    courseTeachService.selectCount(new EntityWrapper<CourseTeach>().eq("teacherid",teacherInfo.getId()))==0){
                userService.deleteById(userid);
                if(exerciseInfoService.selectCount(new EntityWrapper<ExerciseInfo>().eq("creatorid",teacherInfo.getId()))!=0){
                    List<ExerciseInfo> exerciseInfoList = exerciseInfoService.selectList(new EntityWrapper<ExerciseInfo>().eq("creatorid",teacherInfo.getId()));
                    for(ExerciseInfo exerciseInfo : exerciseInfoList){
                        exerciseInfo.setCreatorid(0);
                        exerciseInfoService.updateById(exerciseInfo);
                    }
                }
                teacherInfoService.deleteById(teacherInfo.getId());
                return R.ok("删除成功");
            }else{
                return R.error("该老师创建过课程或者有过授课班，不能删除");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("程序异常");
        }
    }

}

