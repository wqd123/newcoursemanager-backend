package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * @Author: TateBrown
 * @date: 2018/12/24 17:55
 * @param:
 * @return:
 */
public class ExamCourseteach implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer examid;
    private Integer courseteachid;


    //附加字段
    @TableField(exist = false)
    private CourseTeach courseTeach;
    @TableField(exist = false)
    private Exam exam;

    public CourseTeach getCourseTeach() {
        return courseTeach;
    }

    public void setCourseTeach(CourseTeach courseTeach) {
        this.courseTeach = courseTeach;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getExamid() {
        return examid;
    }

    public void setExamid(Integer examid) {
        this.examid = examid;
    }

    public Integer getCourseteachid() {
        return courseteachid;
    }

    public void setCourseteachid(Integer courseteachid) {
        this.courseteachid = courseteachid;
    }
}
