package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class ExerciseInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer type;
    private Integer dlevel;
    private Integer courseid;
    private String content;
    private String answer;
    private String comment;
    private Integer creatorid;
    private Integer isready;
    private Integer isremove;

    public Integer getIsremove() {
        return isremove;
    }

    public void setIsremove(Integer isremove) {
        this.isremove = isremove;
    }

    public Integer getIsready() {
        return isready;
    }

    public void setIsready(Integer isready) {
        this.isready = isready;
    }

    public Integer getCreatorid() {
        return creatorid;
    }

    public void setCreatorid(Integer creatorid) {
        this.creatorid = creatorid;
    }


    @TableField(exist = false)
    private String studentAnswer;

    public String getStudentAnswer() {
        return studentAnswer;
    }

    public void setStudentAnswer(String studentAnswer) {
        this.studentAnswer = studentAnswer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getDlevel() {
        return dlevel;
    }

    public void setDlevel(Integer dlevel) {
        this.dlevel = dlevel;
    }

    public Integer getCourseid() {
        return courseid;
    }

    public void setCourseid(Integer courseid) {
        this.courseid = courseid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "ExerciseInfo{" +
                "id=" + id +
                ", type=" + type +
                ", dlevel=" + dlevel +
                ", courseid=" + courseid +
                ", content='" + content + '\'' +
                ", answer='" + answer + '\'' +
                ", comment='" + comment + '\'' +
                ", creatorid=" + creatorid +
                ", isready=" + isready +
                ", studentAnswer='" + studentAnswer + '\'' +
                '}';
    }
}
