package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.hznu.coursemanagerbackend.modules.sys.entity.PaperShare;
import com.hznu.coursemanagerbackend.modules.sys.dao.PaperShareDao;
import com.hznu.coursemanagerbackend.modules.sys.service.PaperShareService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class PaperShareServiceImpl extends ServiceImpl<PaperShareDao, PaperShare> implements PaperShareService {

}
