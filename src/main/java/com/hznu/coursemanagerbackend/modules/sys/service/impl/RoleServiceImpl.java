package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.hznu.coursemanagerbackend.modules.sys.entity.Role;
import com.hznu.coursemanagerbackend.modules.sys.dao.RoleDao;
import com.hznu.coursemanagerbackend.modules.sys.service.RoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleDao, Role> implements RoleService {

}
