package com.hznu.coursemanagerbackend.modules.sys.bean;

public class ExerciseInfoDto {

    private Integer creatorid;

    public Integer getCreatorid() {
        return creatorid;
    }

    public void setCreatorid(Integer creatorid) {
        this.creatorid = creatorid;
    }

    //关联知识点
    private Integer[] Kpoints;


    public Integer[] getKpoints() {
        return Kpoints;
    }

    public void setKpoints(Integer[] kpoints) {
        Kpoints = kpoints;
    }

    public Integer getDlevel() {
        return dlevel;
    }

    public void setDlevel(Integer dlevel) {
        this.dlevel = dlevel;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCourseid() {
        return courseid;
    }

    public void setCourseid(Integer courseid) {
        this.courseid = courseid;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    private Integer dlevel;

    private Integer type;

    private Integer courseid;

    private String answer;

    private String content;

    private String comment;

    private Integer isready;

    public Integer getIsready() {
        return isready;
    }

    public void setIsready(Integer isready) {
        this.isready = isready;
    }
}
