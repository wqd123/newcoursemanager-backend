package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.common.utils.BeanUtils;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.entity.Course;
import com.hznu.coursemanagerbackend.modules.sys.entity.CourseTeach;
import com.hznu.coursemanagerbackend.modules.sys.dao.CourseTeachDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.TeacherInfo;
import com.hznu.coursemanagerbackend.modules.sys.entity.User;
import com.hznu.coursemanagerbackend.modules.sys.service.CourseService;
import com.hznu.coursemanagerbackend.modules.sys.service.CourseTeachService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hznu.coursemanagerbackend.modules.sys.service.TeacherInfoService;
import com.hznu.coursemanagerbackend.modules.sys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class CourseTeachServiceImpl extends ServiceImpl<CourseTeachDao, CourseTeach> implements CourseTeachService {

    @Autowired
    CourseService courseService;
    @Autowired
    TeacherInfoService teacherInfoService;
    @Autowired
    UserService userService;

    @Override
    public PageUtils queryPage(Map<String,Object> params)
    {
        String keyword = (String)params.get("keyword");
        Page<CourseTeach> page = this.selectPage(
                new Query<CourseTeach>(params).getPage(),
                new EntityWrapper<CourseTeach>().allEq(BeanUtils.convertBeanToMap(new CourseTeach(),params)).andNew(keyword!=null,null).
                        like(keyword!=null,"nickname",keyword).or(keyword!=null,null).
                        like(keyword!=null,"address",keyword)
        );
        for(CourseTeach courseTeach : page.getRecords())
        {
            Course course = courseService.findById(courseTeach.getCourseid());
            courseTeach.setCourse(course);
        }
        return new PageUtils(page);
    }

    @Override
    public void modify(CourseTeach courseTeach)
    {
        this.updateById(courseTeach);
    }

    @Override
    public List<CourseTeach> findTeacherListByCourseid(Integer courseid)
    {
        List<CourseTeach> courseTeaches = this.selectList(new EntityWrapper<CourseTeach>().eq("courseid",courseid));
        for(CourseTeach courseTeach:courseTeaches)
        {
            TeacherInfo teacherInfo = teacherInfoService.selectById(courseTeach.getTeacherid());
            teacherInfo.setUser(userService.selectById(teacherInfo.getUserid()));
            courseTeach.setTeacher(teacherInfo);
        }
        return courseTeaches;
    }

}
