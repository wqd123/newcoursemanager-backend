package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.hznu.coursemanagerbackend.myBeans.TreeSupport;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class Department extends TreeSupport implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private Integer schoolid;
    private Integer universityid;
    private Integer type;

    //附加字段
    @TableField(exist = false)
    private List<Major> children;
    @TableField(exist = false)
    private final int adminrank = 3;
    @TableField(exist = false)
    private School school;

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public List<Major> getChildren() {
        return children;
    }

    public void setChildren(List<Major> children) {
        this.children = children;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Integer schoolid) {
        this.schoolid = schoolid;
    }

    public Integer getUniversityid() {
        return universityid;
    }

    public void setUniversityid(Integer universityid) {
        this.universityid = universityid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public int getAdminrank() {
        return adminrank;
    }

    @Override
    public String toString() {
        return "Major{" +
        ", id=" + id +
        ", name=" + name +
        ", schoolid=" + schoolid +
        ", universityid=" + universityid +
        ", type=" + type +
        "}";
    }
}
