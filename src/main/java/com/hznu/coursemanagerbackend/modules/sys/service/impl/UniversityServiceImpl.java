package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.hznu.coursemanagerbackend.modules.sys.entity.University;
import com.hznu.coursemanagerbackend.modules.sys.dao.UniversityDao;
import com.hznu.coursemanagerbackend.modules.sys.service.UniversityService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class UniversityServiceImpl extends ServiceImpl<UniversityDao, University> implements UniversityService {

}
