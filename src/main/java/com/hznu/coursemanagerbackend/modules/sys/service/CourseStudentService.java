package com.hznu.coursemanagerbackend.modules.sys.service;

import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.entity.Attendexam;
import com.hznu.coursemanagerbackend.modules.sys.entity.CourseStudent;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface CourseStudentService extends IService<CourseStudent> {

    PageUtils queryPageByCourseteachid(Map<String,Object> params);

    Integer LoadBatch(List<Object> objectList, Integer courseTeachid);
}
