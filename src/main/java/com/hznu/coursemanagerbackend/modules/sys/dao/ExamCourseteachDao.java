package com.hznu.coursemanagerbackend.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExamCourseteach;

/**
 * @Author: TateBrown
 * @date: 2018/12/24 17:58
 * @param:
 * @return:
 */
public interface ExamCourseteachDao extends BaseMapper<ExamCourseteach> {
}
