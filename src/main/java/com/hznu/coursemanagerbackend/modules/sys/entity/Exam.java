package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class Exam implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private String place;
    private Date examdate;
    private Date starttime;
    private Date finishtime;
    private Integer exampaperid;
    private Integer nop;
    private String invigilator1;
    private String invigilator2;
    private Integer state;
    private Integer duration;

    //附加字段
    @TableField(exist = false)
    private int courseteachid;

    public int getCourseteachid() {
        return courseteachid;
    }

    public void setCourseteachid(int courseteachid) {
        this.courseteachid = courseteachid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Date getExamdate() {
        return examdate;
    }

    public void setExamdate(Date examdate) {
        this.examdate = examdate;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getFinishtime() {
        return finishtime;
    }

    public void setFinishtime(Date finishtime) {
        this.finishtime = finishtime;
    }

    public Integer getExampaperid() {
        return exampaperid;
    }

    public void setExampaperid(Integer exampaperid) {
        this.exampaperid = exampaperid;
    }

    public Integer getNop() {
        return nop;
    }

    public void setNop(Integer nop) {
        this.nop = nop;
    }

    public String getInvigilator1() {
        return invigilator1;
    }

    public void setInvigilator1(String invigilator1) {
        this.invigilator1 = invigilator1;
    }

    public String getInvigilator2() {
        return invigilator2;
    }

    public void setInvigilator2(String invigilator2) {
        this.invigilator2 = invigilator2;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Exam{" +
        ", id=" + id +
        ", name=" + name +
        ", place=" + place +
        ", examdate=" + examdate +
        ", starttime=" + starttime +
        ", finishtime=" + finishtime +
        ", exampaperid=" + exampaperid +
        ", nop=" + nop +
        ", invigilator1=" + invigilator1 +
        ", invigilator2=" + invigilator2 +
        ", state=" + state +
        ", duration=" + duration +
        "}";
    }
}
