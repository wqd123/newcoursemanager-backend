package com.hznu.coursemanagerbackend.modules.sys.service;

import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseCollection;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2019-03-01
 */
public interface ExerciseCollectionService extends IService<ExerciseCollection> {

}
