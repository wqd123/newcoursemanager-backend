package com.hznu.coursemanagerbackend.modules.sys.service;

import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.entity.CourseTeach;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface CourseTeachService extends IService<CourseTeach> {

    PageUtils queryPage(Map<String,Object> params);

    void modify(CourseTeach courseTeach);

    List<CourseTeach> findTeacherListByCourseid(Integer courseid);
}
