package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class PaperShare implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer exampaperid;
    private Integer teacherid;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getExampaperid() {
        return exampaperid;
    }

    public void setExampaperid(Integer exampaperid) {
        this.exampaperid = exampaperid;
    }

    public Integer getTeacherid() {
        return teacherid;
    }

    public void setTeacherid(Integer teacherid) {
        this.teacherid = teacherid;
    }

    @Override
    public String toString() {
        return "PaperShare{" +
        ", id=" + id +
        ", exampaperid=" + exampaperid +
        ", teacherid=" + teacherid +
        "}";
    }
}
