package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class ExerkPoint implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer exerinfoid;
    private Integer kpointid;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getExerinfoid() {
        return exerinfoid;
    }

    public void setExerinfoid(Integer exerinfoid) {
        this.exerinfoid = exerinfoid;
    }

    public Integer getKpointid() {
        return kpointid;
    }

    public void setKpointid(Integer kpointid) {
        this.kpointid = kpointid;
    }

    @Override
    public String toString() {
        return "ExerkPoint{" +
        ", id=" + id +
        ", exerinfoid=" + exerinfoid +
        ", kpointid=" + kpointid +
        "}";
    }
}
