package com.hznu.coursemanagerbackend.modules.sys.service.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.dao.PaperTitleModelDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.PaperTitleModel;
import com.hznu.coursemanagerbackend.modules.sys.service.PaperTitleModelService;

import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2019-04-02
 */
@Service
public class PaperTitleModelServiceImpl extends ServiceImpl<PaperTitleModelDao, PaperTitleModel> implements PaperTitleModelService {
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String keyword = (String) params.get("keyword");
        Page<PaperTitleModel> page = this.selectPage(new Query<PaperTitleModel>(params).getPage(),
                new EntityWrapper<PaperTitleModel>().eq(keyword.equals("")!=true,"name",keyword));
        return new PageUtils(page);
    }
}
