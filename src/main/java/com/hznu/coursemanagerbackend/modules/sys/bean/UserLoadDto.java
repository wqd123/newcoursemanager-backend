package com.hznu.coursemanagerbackend.modules.sys.bean;

import com.xuxueli.poi.excel.annotation.ExcelField;
import com.xuxueli.poi.excel.annotation.ExcelSheet;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * @Author: TateBrown
 * @date: 2018/12/24 20:05
 用户批量导入DTO
 */
@ExcelSheet(name="Sheet1",headColor = HSSFColor.HSSFColorPredefined.LIGHT_GREEN)
public class UserLoadDto {
    @ExcelField(name="工号或学号")
    private String workId;
    @ExcelField(name="真实姓名")
    private String realName;
    @ExcelField(name="性别(男用1表示女用2表示)")
    private String sex;

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
