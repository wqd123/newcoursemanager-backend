package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.validator.ValidatorUtils;
import com.hznu.coursemanagerbackend.common.validator.group.AddGroup;
import com.hznu.coursemanagerbackend.modules.sys.entity.Class;
import com.hznu.coursemanagerbackend.modules.sys.entity.Department;
import com.hznu.coursemanagerbackend.modules.sys.entity.Major;
import com.hznu.coursemanagerbackend.modules.sys.entity.School;
import com.hznu.coursemanagerbackend.modules.sys.service.ClassService;
import com.hznu.coursemanagerbackend.modules.sys.service.DepartmentService;
import com.hznu.coursemanagerbackend.modules.sys.service.MajorService;
import com.hznu.coursemanagerbackend.modules.sys.service.SchoolService;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/school")
@Api(description = "学院接口",value = "school")
public class SchoolController {

    @Autowired
    SchoolService schoolService;
    @Autowired
    DepartmentService departmentService;
    @Autowired
    MajorService majorService;
    @Autowired
    ClassService classService;

    /**
     * @author Wqd
     * @since 2018-12-1
     */
    @ApiOperation("添加学院")
    @PostMapping("/add")
    public R add(@RequestBody School school){
        try{
            schoolService.insert(school);
            return R.ok();
        }catch (Exception e){
            e.printStackTrace();
            return R.error();
        }
    }

    @ApiOperation("查询所有学员")
    @GetMapping("/listAll")
    public R listAll()
    {
        List<School> schoolList = schoolService.selectList(new EntityWrapper<School>());
        return R.ok().put("schoolList",schoolList);
    }

    @ApiOperation("院系专业班级结构树")
    @GetMapping("/getSchoolMajorClassTree/{rank}")
    public R getTree(@PathVariable Integer rank){
        try{
            if(rank >= 2) {
                List<School> schoolList = schoolService.selectList(new EntityWrapper<School>());
                for (School school : schoolList) {
                    school.setParentId(null);
                    school.setTreeKey("school"+school.getId());
                    if(rank >= 3) {
                        List<Department> departmentList = departmentService.selectList(new EntityWrapper<Department>().eq("schoolid",school.getId()));
                        for (Department department : departmentList) {
                            department.setTreeKey("department"+department.getId());
                            department.setParentId(school.getTreeKey());
                            if(rank >= 4) {
                                List<Major> majorList = majorService.selectList(new EntityWrapper<Major>().eq("departmentid",department.getId()));
                                for (Major major : majorList) {
                                    major.setTreeKey("major"+major.getId());
                                    major.setParentId(department.getTreeKey());
                                    if(rank >= 5) {
                                        List<Class> classList = classService.selectList(new EntityWrapper<Class>().eq("majorid",major.getId()));
                                        for (Class clazz : classList) {
                                            clazz.setTreeKey("class"+clazz.getId());
                                            clazz.setParentId(major.getTreeKey());
                                        }
                                        major.setChildren(classList);
                                    }
                                }
                                department.setChildren(majorList);
                            }
                        }
                        school.setChildren(departmentList);
                    }
                }
                return R.ok().put("data",schoolList);
            }else {
                return R.error("无数据");
            }
        }catch (Exception e){
            e.printStackTrace();
            return R.error();
        }
    }

    /**
     * @author Wqd
     * @since 2018-12-3
     */
    @ApiOperation("返回所有学院")
    @GetMapping("/findAllschool")
    public R findAllschool() {
        try {
            return R.ok("获取成功").put("data", schoolService.selectList(new EntityWrapper<School>()));
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("程序异常");
        }
    }

    /**
     * @author Wqd
     * @since 2019-3-28
     */
    @ApiOperation("删除学院")
    @GetMapping("/removeschool/{id}")
    public R removeschool(@PathVariable Integer id) {
        try {
            if(departmentService.selectList(new EntityWrapper<Department>().eq("schoolid",id)).size()==0){
                return R.ok("删除成功").put("data", schoolService.deleteById(id));
            }else{
                return R.error("学院下有系部，不能删除");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("程序异常");
        }
    }

    /**
     * @author Wqd
     * @since 2019-3-31
     */
    @ApiOperation("条件查询学院（分页）")
    @PostMapping("/list")
    public R list(@RequestParam Map<String,Object> params)
    {
        PageUtils page = schoolService.queryPage(params);
        return R.ok().put("page",page);
    }

    @ApiOperation("修改学院信息")
    @PostMapping("/modify")
    public R modify(@RequestBody School school) {
        ValidatorUtils.validateEntity(school, AddGroup.class);
        schoolService.updateById(school);
        return R.ok();
    }

}

