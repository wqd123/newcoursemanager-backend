package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.entity.StudentInfo;
import com.hznu.coursemanagerbackend.modules.sys.dao.StudentInfoDao;
import com.hznu.coursemanagerbackend.modules.sys.service.ClassService;
import com.hznu.coursemanagerbackend.modules.sys.service.StudentInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hznu.coursemanagerbackend.modules.sys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class StudentInfoServiceImpl extends ServiceImpl<StudentInfoDao, StudentInfo> implements StudentInfoService {

    @Autowired
    UserService userService;

    @Override
    public StudentInfo studnetDetail(Integer id)
    {
        StudentInfo studentInfo = this.selectById(id);
        studentInfo.setUser(userService.selectById(studentInfo.getUserid()));
        return studentInfo;
    }

}
