package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.entity.ComposePaper;
import com.hznu.coursemanagerbackend.modules.sys.dao.ComposePaperDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseInfo;
import com.hznu.coursemanagerbackend.modules.sys.service.ComposePaperService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hznu.coursemanagerbackend.modules.sys.service.ExerciseInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class ComposePaperServiceImpl extends ServiceImpl<ComposePaperDao, ComposePaper> implements ComposePaperService {

    @Autowired
    ExerciseInfoService exerciseInfoService;


    @Override
    public List<ComposePaper> findByPaperid(Integer paperid)
    {
        List<ComposePaper> composePapers = this.selectList(new EntityWrapper<ComposePaper>().eq("paperid",paperid));
        for(ComposePaper composePaper : composePapers)
        {
            composePaper.setExerciseInfo(exerciseInfoService.selectById(composePaper.getExerinfoid()));
        }
        return composePapers;
    }

    @Override
    public void addExerandCompose(ComposePaper composePaper)
    {
        exerciseInfoService.insert(composePaper.getExerciseInfo());
        composePaper.setExerinfoid(composePaper.getExerciseInfo().getId());
        this.insert(composePaper);
    }

    @Override
    public PageUtils findByPaperid(Map<String,Object> params)
    {
        int paperid = Integer.parseInt((String)params.get("paperid"));
        Page<ComposePaper> page = this.selectPage(new Query<ComposePaper>(params).getPage(),
                new EntityWrapper<ComposePaper>().eq("paperid",paperid));
        for(ComposePaper composePaper:page.getRecords())
        {
            composePaper.setExerciseInfo(exerciseInfoService.selectById(composePaper.getExerinfoid()));
        }
        return new PageUtils(page);
    }
}
