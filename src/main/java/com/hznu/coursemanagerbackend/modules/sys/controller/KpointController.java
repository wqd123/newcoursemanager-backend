package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.validator.ValidatorUtils;
import com.hznu.coursemanagerbackend.common.validator.group.AddGroup;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerkPoint;
import com.hznu.coursemanagerbackend.modules.sys.entity.Kpoint;
import com.hznu.coursemanagerbackend.modules.sys.service.KpointService;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/kpoint")
@Api(description = "知识点相关接口",value = "kpoint")
public class KpointController {

    @Autowired
    KpointService kpointService;

    /**
     * @author Wqd
     * @since 2018-12-3
     */
    @ApiOperation("添加知识点")
    @PostMapping("/add")
    public R add(@RequestBody Kpoint kpoint){
        try{
            if(kpoint.getParentid()==null){
                kpoint.setParentid(-1);
            }
            if(kpointService.selectOne(new EntityWrapper<Kpoint>().eq("name",kpoint.getName()))!=null){
                return R.error("该知识点已存在");
            }
            kpointService.insert(kpoint);
            return R.ok();
        }catch (Exception e){
            e.printStackTrace();
            return R.error();
        }
    }

    @ApiOperation("删除知识点（若为父节点则连带子节点删除）")
    @GetMapping("/removeById/{id}")
    public R removeById(@PathVariable Integer id){
        try{
            kpointService.removeById(id);
            return R.ok("删除成功");
        }catch (Exception e){
            e.printStackTrace();
            return R.error();
        }
    }

    @ApiOperation("修改知识点树")
    @PostMapping("/kpoint/modify")
    public R modify(@RequestBody Kpoint kpoint) {
        ValidatorUtils.validateEntity(kpoint, AddGroup.class);
        kpointService.updateById(kpoint);
        return R.ok();
    }

    /**
     * @author Wqd
     * @since 2018-12-4
     */
    @ApiOperation("条件查询知识点分页")
    @PostMapping("/list")
    public R list(@RequestBody Map<String,Object> params){
        PageUtils page = kpointService.queryPage(params);
        return R.ok().put("page",page);
    }

    @ApiOperation("根据课程id返回知识点树")
    @PostMapping("/getKpointTree/{courseid}")
    public R navTree(@PathVariable Integer courseid){
        try{
            Kpoint kpoint = new Kpoint();
            kpoint.setCourseid(courseid);
            kpoint.setParentid(-1);
            List<Kpoint> kpointList = kpointService.selectList(
                    new EntityWrapper<Kpoint>().eq("courseid",kpoint.getCourseid()).and().
                            eq("parentid",kpoint.getParentid())
            );
            Collections.sort(kpointList);
            for (Kpoint kpoint1:kpointList){
                kpoint1.setChildren(kpointService.queryListByPid(kpoint1.getId()));
            }
            return R.ok().put("data",kpointList);
        }catch (Exception e){
            e.printStackTrace();
            return R.error(e.toString());
        }
    }

}

