package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.hznu.coursemanagerbackend.modules.sys.entity.ExerkPoint;
import com.hznu.coursemanagerbackend.modules.sys.dao.ExerkPointDao;
import com.hznu.coursemanagerbackend.modules.sys.service.ExerkPointService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class ExerkPointServiceImpl extends ServiceImpl<ExerkPointDao, ExerkPoint> implements ExerkPointService {

}
