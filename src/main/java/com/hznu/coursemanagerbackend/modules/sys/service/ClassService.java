package com.hznu.coursemanagerbackend.modules.sys.service;

import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.entity.Class;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface ClassService extends IService<Class> {

    List<Class> findByName(Class clazz);

    List<Class> selectBykeyword(String name);

    PageUtils queryPage(Map<String,Object> params);

    PageUtils queryPageByMajorId(Map<String,Object> params);

}
