package com.hznu.coursemanagerbackend.modules.sys.service;

import com.alibaba.druid.sql.PagerUtils;
import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.entity.Course;
import com.baomidou.mybatisplus.service.IService;
import com.hznu.coursemanagerbackend.modules.sys.entity.TeacherInfo;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface CourseService extends IService<Course> {

    PageUtils queryPage(Map<String,Object> params);

    Course findById(Integer id);
}
