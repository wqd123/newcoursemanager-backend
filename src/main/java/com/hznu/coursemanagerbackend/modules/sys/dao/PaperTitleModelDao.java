package com.hznu.coursemanagerbackend.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hznu.coursemanagerbackend.modules.sys.entity.PaperTitleModel;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DragonDoor
 * @since 2019-04-17
 */
public interface PaperTitleModelDao extends BaseMapper<PaperTitleModel> {

}
