package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.hznu.coursemanagerbackend.modules.sys.entity.PaperexertypeInfo;
import com.hznu.coursemanagerbackend.modules.sys.dao.PaperexertypeInfoDao;
import com.hznu.coursemanagerbackend.modules.sys.service.PaperexertypeInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class PaperexertypeInfoServiceImpl extends ServiceImpl<PaperexertypeInfoDao, PaperexertypeInfo> implements PaperexertypeInfoService {

}
