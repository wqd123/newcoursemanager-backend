package com.hznu.coursemanagerbackend.modules.sys.service;

import com.hznu.coursemanagerbackend.modules.sys.entity.Attendexam;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExamRecord;
import com.baomidou.mybatisplus.service.IService;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface ExamRecordService extends IService<ExamRecord> {

    void addRecords(int examid, int studentid, List<ExerciseInfo> exerciseInfos);
    List<ExamRecord> studentAnswers(Integer examid,Integer studeintid);
    void modifyScores(List<ExamRecord>examRecords);
    void markByExamCourseteachid(Integer examCourseteachid);
}
