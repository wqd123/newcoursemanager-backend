package com.hznu.coursemanagerbackend.modules.sys.service;

import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.entity.ComposePaper;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface ComposePaperService extends IService<ComposePaper> {

    List<ComposePaper> findByPaperid(Integer paperid);

    void addExerandCompose(ComposePaper composePaper);

    PageUtils findByPaperid(Map<String,Object> params);
}
