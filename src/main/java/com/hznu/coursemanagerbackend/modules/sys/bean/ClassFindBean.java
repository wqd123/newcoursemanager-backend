package com.hznu.coursemanagerbackend.modules.sys.bean;

public class ClassFindBean {

    private String name;

    private Integer teacherid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTeacherid() {
        return teacherid;
    }

    public void setTeacherid(Integer teacherid) {
        this.teacherid = teacherid;
    }
}
