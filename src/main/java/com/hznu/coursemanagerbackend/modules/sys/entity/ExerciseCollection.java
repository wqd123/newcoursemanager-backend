package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2019-03-01
 */
public class ExerciseCollection implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 记录id
     */
    private Integer id;
    /**
     * 学生id
     */
    private Integer studentid;
    /**
     * 考试id
     */
    private Integer exerciseinfoid;

    private Integer courseid;

    public Integer getCourseid() {
        return courseid;
    }

    public void setCourseid(Integer courseid) {
        this.courseid = courseid;
    }

    //附加字段
    @TableField(exist = false)
    private ExerciseInfo exerciseInfo;

    public ExerciseInfo getExerciseInfo() {
        return exerciseInfo;
    }

    public void setExerciseInfo(ExerciseInfo exerciseInfo) {
        this.exerciseInfo = exerciseInfo;
    }

    public Integer getExerciseinfoid() {
        return exerciseinfoid;
    }

    public void setExerciseinfoid(Integer exerciseinfoid) {
        this.exerciseinfoid = exerciseinfoid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudentid() {
        return studentid;
    }

    public void setStudentid(Integer studentid) {
        this.studentid = studentid;
    }


    @Override
    public String toString() {
        return "ExerciseCollection{" +
        ", id=" + id +
        ", studentid=" + studentid +
        ", examid=" + exerciseinfoid +
        "}";
    }
}
