package com.hznu.coursemanagerbackend.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExamCourseteach;

import java.util.List;

/**
 * @Author: TateBrown
 * @date: 2018/12/24 18:00
 * @param:
 * @return:
 */
public interface ExamCourseteachService extends IService<ExamCourseteach> {
    List<ExamCourseteach> courseteachList(Integer examid);
}
