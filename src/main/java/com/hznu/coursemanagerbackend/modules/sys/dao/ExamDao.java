package com.hznu.coursemanagerbackend.modules.sys.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.modules.sys.entity.Exam;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface ExamDao extends BaseMapper<Exam> {
    List<Exam> findByCondition(Page<Exam> page, @Param("exam")Exam courseStudent, @Param("keyword") String keyword);

    List<Exam>findByCourseteachid(Page<Exam>page,@Param("courseteachid") Integer courseteachid,@Param("keyword") String keyword);
}
