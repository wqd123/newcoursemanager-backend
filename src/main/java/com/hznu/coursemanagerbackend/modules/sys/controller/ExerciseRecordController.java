package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.hznu.coursemanagerbackend.modules.sys.entity.AnswerRecord;
import com.hznu.coursemanagerbackend.modules.sys.service.ExerciseRecordService;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/exerciseRecord")
public class ExerciseRecordController {

    @Autowired
    ExerciseRecordService exerciseRecordService;

    @ApiOperation("单个添加练习答案记录")
    @PostMapping("/addOne")
    public R addOne(@RequestBody AnswerRecord answerRecord)
    {
        try
        {
            exerciseRecordService.addOne(answerRecord);
            return R.ok();
        }catch (Exception e)
        {
            e.printStackTrace();
            return R.error();
        }
    }
}

