package com.hznu.coursemanagerbackend.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.hznu.coursemanagerbackend.modules.sys.entity.AnswerRecord;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface AnswerRecordService extends IService<AnswerRecord> {

}
