package com.hznu.coursemanagerbackend.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.bean.ExamDetail;
import com.hznu.coursemanagerbackend.modules.sys.entity.Attendexam;
import com.hznu.coursemanagerbackend.modules.sys.entity.Exam;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface AttendexamService extends IService<Attendexam> {

    PageUtils queryPageByExamId(Map<String,Object> params);

    List<ExamDetail> findExamsByStudentid(Integer studentid);

    List<Attendexam> findStudentByExamidAndCourseteachid(Integer examid,Integer courseteachid);

    void initRemain(Exam exam);

    Attendexam remainReduce(Integer id,Integer sec);
}
