package com.hznu.coursemanagerbackend.modules.sys.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.validator.ValidatorUtils;
import com.hznu.coursemanagerbackend.common.validator.group.AddGroup;
import com.hznu.coursemanagerbackend.modules.sys.entity.*;
import com.hznu.coursemanagerbackend.modules.sys.service.*;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/exam")
@Api(description = "考试接口",value = "exam")
public class ExamController {

    @Autowired
    ExamService examService;

    @Autowired
    ExerciseInfoService exerciseInfoService;

    @Autowired
    ComposePaperService composePaperService;

    @Autowired
    ExamCourseteachService examCourseteachService;

    @Autowired
    AttendexamService attendexamService;

    /**
     * @author Wqd
     * @since 2018-12-5
     */
    @ApiOperation("添加考试")
    @PostMapping("/add")
    public R add(@RequestBody Exam exam)
    {
        try
        {
            examService.add(exam);
            ExamCourseteach examCourseteach = new ExamCourseteach();
            examCourseteach.setExamid(exam.getId());
            examCourseteach.setCourseteachid(exam.getCourseteachid());
            examCourseteachService.insert(examCourseteach);
            return R.ok("添加成功！");
        }catch (Exception e){
            e.printStackTrace();
            return R.error();
        }
    }

    /**
     * @author Wqd
     * @since 2018-12-18
     */
    @ApiOperation("修改考试信息")
    @PostMapping("/modify")
    public R modifyexam(@RequestBody Exam exam) {
        try {
            ValidatorUtils.validateEntity(exam, AddGroup.class);
            examService.updateById(exam);
            return R.ok("修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.error();
        }
    }

//    @ApiOperation("按照授课id及关键字条件查找考试")
//    @PostMapping("/findExamsByCondition")
//    public R findStudentsByCondition(@RequestParam Map<String,Object> params) {
//        PageUtils page = examService.queryPageByCourseteachid(params);
//        return R.ok().put("page", page);
//    }

    @ApiOperation("按照授课id及关键字条件查找考试")
    @PostMapping("/findByCourseteachid")
    public R findByCourseteachid(@RequestParam Map<String,Object> params)
    {
        PageUtils page = examService.queryPageByCourseteachid(params);
        return R.ok().put("page",page);
    }

    /**
     * @author Wqd
     * @since 2019-1-18
     */
    @ApiOperation("通过考试ID查找所有题目")
    @PostMapping("/findExerciseDetailByexamid/{id}")
    public R findExerciseDetailByexamid(@PathVariable Integer id)
    {
        try
        {
            Exam exam = examService.selectById(id);
            List<ComposePaper> composePapers = composePaperService.selectList(new EntityWrapper<ComposePaper>().eq("paperid",exam.getExampaperid()));
            List<ExerciseInfo> exerciseInfoList = new LinkedList<>();
            for(ComposePaper composePaper : composePapers){
                ExerciseInfo exerciseInfo = exerciseInfoService.selectById(composePaper.getExerinfoid());
                exerciseInfo.setAnswer(null);
                exerciseInfo.setComment(null);
                exerciseInfo.setDlevel(null);
                exerciseInfoList.add(exerciseInfo);
            }
            return R.ok().put("data",exerciseInfoList);
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("查找失败");
        }
    }

    /**
     * @author Wqd
     * @since 2019-3-29
     */
    @ApiOperation("删除考试")
    @GetMapping("/removeExam/{id}")
    public R removeExam(@PathVariable Integer id) {
        try {
            if(examService.selectById(id).getState()==1){
                List<Attendexam> attendexamList = attendexamService.selectList(new EntityWrapper<Attendexam>().eq("examid",id));
                for(Attendexam attendexam : attendexamList){
                    attendexamService.deleteById(attendexam.getId());
                }
                examService.deleteById(id);
                return R.ok("删除成功");
            }else{
                return R.error("该考试正在进行中或者已经结束，不能删除");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("程序异常");
        }
    }

}

