package com.hznu.coursemanagerbackend.modules.sys.bean;

import com.hznu.coursemanagerbackend.modules.sys.entity.CourseStudent;
import com.xuxueli.poi.excel.annotation.ExcelField;
import com.xuxueli.poi.excel.annotation.ExcelSheet;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * @Author: TateBrown
 * @date: 2018/12/24 21:39
 * @param:
 * @return:
 */
@ExcelSheet(name="Sheet1",headColor = HSSFColor.HSSFColorPredefined.LIGHT_GREEN)
public class CourseStudentLoadDto extends CourseStudent{
    @ExcelField(name="学号")
    private String workId;

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }
}
