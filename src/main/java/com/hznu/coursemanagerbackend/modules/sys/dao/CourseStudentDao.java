package com.hznu.coursemanagerbackend.modules.sys.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.modules.sys.entity.CourseStudent;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface CourseStudentDao extends BaseMapper<CourseStudent> {

    List<CourseStudent> findByCondition(Page<CourseStudent> page, @Param("courseStudent")CourseStudent courseStudent, @Param("keyword") String keyword);

}
