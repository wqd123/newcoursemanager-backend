package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hznu.coursemanagerbackend.modules.sys.dao.ExamCourseteachDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExamCourseteach;
import com.hznu.coursemanagerbackend.modules.sys.service.CourseTeachService;
import com.hznu.coursemanagerbackend.modules.sys.service.ExamCourseteachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: TateBrown
 * @date: 2018/12/24 18:01
 * @param:
 * @return:
 */
@Service
public class ExamCourseteachServiceImpl extends ServiceImpl<ExamCourseteachDao, ExamCourseteach> implements ExamCourseteachService {

    @Autowired
    CourseTeachService courseTeachService;

    @Override
    public List<ExamCourseteach> courseteachList(Integer examid)
    {
        List<ExamCourseteach> examCourseteaches = this.selectList(new EntityWrapper<ExamCourseteach>().eq("examid",examid));
        for(ExamCourseteach examCourseteach : examCourseteaches)
        {
            examCourseteach.setCourseTeach(courseTeachService.selectById(examCourseteach.getCourseteachid()));
        }
        return examCourseteaches;
    }

}
