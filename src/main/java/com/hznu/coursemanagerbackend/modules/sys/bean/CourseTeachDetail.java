package com.hznu.coursemanagerbackend.modules.sys.bean;

import com.hznu.coursemanagerbackend.modules.sys.entity.CourseTeach;

/**
 * @Author: TateBrown
 * @date: 2018/12/4 11:27
 * @param:
 * @return:
 */
public class CourseTeachDetail extends CourseTeach {
    private String courseName;

    private String createRealname;

    public String getCourseName() {
        return courseName;
    }

    public String getCreateRealname() {
        return createRealname;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setCreateRealname(String createRealname) {
        this.createRealname = createRealname;
    }
}
