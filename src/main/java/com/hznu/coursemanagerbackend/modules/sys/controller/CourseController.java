package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.entity.*;
import com.hznu.coursemanagerbackend.modules.sys.service.*;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/course")
@Api(description = "课程管理接口",value = "course")
public class CourseController extends AbstractController{

    @Autowired
    CourseService courseService;

    @Autowired
    TeacherInfoService teacherInfoService;

    @Autowired
    CourseTeachService courseTeachService;

    @Autowired
    UserService userService;

    @Autowired
    KpointService kpointService;

    @Autowired
    ExerciseInfoService exerciseInfoService;

    @ApiOperation("添加课程")
    @PostMapping("/add")
    public R add(@RequestBody Course course)
    {
        try
        {
            if(courseService.selectOne(new EntityWrapper<Course>().eq("name",course.getName()))==null)
            {
                courseService.insert(course);
                return R.ok("课程创建成功");
            }else
            {
                return R.error("已存在相同课程");
            }

        }catch (Exception e)
        {
            e.printStackTrace();
            return R.error();
        }
    }

    @ApiOperation("条件查询课程分页")
    @PostMapping("/list")
    public R list(@RequestParam Map<String,Object> params)
    {
        PageUtils page = courseService.queryPage(params);
        return R.ok().put("page",page);
    }

    /**
     * @Description:修改课程名
     * @param:
     * @return:
     * @author: TateBrown
     * @Date: 2018/12/4
     */

    @ApiOperation("修改课程名")
    @PostMapping("/modify")
    public R modify(@RequestBody Course course){
        try{
            courseService.updateById(course);
            return R.ok("修改成功");
        }catch(Exception e){
            logger.error(e.toString());
            return R.error();
        }
    }

    /**
     * @author Wqd
     * @since 2018-12-26
     */
    @ApiOperation("根据课程名称查找所有下属授课班")
    @PostMapping("/findAllByCourseName")
    public R findAllByCourseName(@RequestBody Course course){
        Course course1 = courseService.selectOne(new EntityWrapper<Course>().eq("name",course.getName()));
        List<CourseTeach> courseTeaches = courseTeachService.selectList(new EntityWrapper<CourseTeach>().eq("courseid",course1.getId()));
        for(CourseTeach courseTeach : courseTeaches){
            TeacherInfo teacherInfo = teacherInfoService.selectById(courseTeach.getTeacherid());
            User user = userService.selectById(teacherInfo.getUserid());
            teacherInfo.setUser(user);
            courseTeach.setTeacher(teacherInfo);
            courseTeach.setCoursename(course.getName());
        }
        return R.ok().put("course",courseTeaches);
    }

    /**
     * @author Wqd
     * @since 2019-3-29
     */
    @ApiOperation("删除课程")
    @GetMapping("/removecourse/{id}")
    public R removecourse(@PathVariable Integer id) {
        try {
            if(courseTeachService.selectCount(new EntityWrapper<CourseTeach>().eq("courseid",id))==0&&
                    kpointService.selectCount(new EntityWrapper<Kpoint>().eq("courseid",id))==0&&
                    exerciseInfoService.selectCount(new EntityWrapper<ExerciseInfo>().eq("courseid",id))==0){
                return R.ok("删除成功").put("data", courseService.deleteById(id));
            }else{
                return R.error("课程下有授课、知识点或者题目，不能删除");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("程序异常");
        }
    }

}

