package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.entity.*;
import com.hznu.coursemanagerbackend.modules.sys.dao.ExamDao;
import com.hznu.coursemanagerbackend.modules.sys.service.AttendexamService;
import com.hznu.coursemanagerbackend.modules.sys.service.CourseStudentService;
import com.hznu.coursemanagerbackend.modules.sys.service.ExamService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class ExamServiceImpl extends ServiceImpl<ExamDao, Exam> implements ExamService {

    @Autowired
    ExamService examService;

    @Autowired
    CourseStudentService courseStudentService;

    @Autowired
    AttendexamService attendexamService;

//    @Override
//    public PageUtils queryPageByCourseteachid(Map<String,Object> params)
//    {
//        Exam exam = new Exam();
//        String keyword = (String)params.get("keyword");
//        Page<Exam> page = new Query<Exam>(params).getPage();
//        List<Exam> exams = this.baseMapper.findByCondition(page,exam,keyword);
//        page.setRecords(exams);
//        return new PageUtils(page);
//    }

    @Override
    public PageUtils queryPageByCourseteachid(Map<String,Object> params)
    {
        String keyword = (String)params.get("keyword");
        Integer courseteachid = Integer.parseInt((String)params.get("courseteachid"));
        Page<Exam> page = new Query<Exam>(params).getPage();
        List<Exam> exams = this.baseMapper.findByCourseteachid(page,courseteachid,keyword);
        page.setRecords(exams);
        return new PageUtils(page);
    }
    @Override
    public void add(Exam exam){
        if(examService.insert(exam)){
            List<CourseStudent> courseStudentList=courseStudentService.selectList(new EntityWrapper<CourseStudent>().eq("courseteachid",exam.getCourseteachid()));
            for(CourseStudent courseStudent : courseStudentList){
                Attendexam attendexam = new Attendexam();
                attendexam.setExamid(exam.getId());
                attendexam.setStudentid(courseStudent.getStudentid());
                attendexamService.insert(attendexam);
            }
        }
    }

}
