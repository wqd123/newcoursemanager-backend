package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.entity.*;
import com.hznu.coursemanagerbackend.modules.sys.service.AttendexamService;
import com.hznu.coursemanagerbackend.modules.sys.service.CourseStudentService;
import com.hznu.coursemanagerbackend.modules.sys.service.StudentInfoService;
import com.hznu.coursemanagerbackend.modules.sys.service.UserService;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/studentInfo")
@Api(description = "学生信息接口",value = "studentInfo")
public class StudentInfoController extends AbstractController{

    @Autowired
    CourseStudentService courseStudentService;

    @Autowired
    UserService userService;

    @Autowired
    StudentInfoService studentInfoService;

    /**
     * @author Wqd
     * @since 2019-3-29
     */
    @ApiOperation("删除学生")
    @GetMapping("/removestudent/{userid}")
    public R removestudent(@PathVariable Integer userid) {
        try {
            StudentInfo studentInfo = studentInfoService.selectOne(new EntityWrapper<StudentInfo>().eq("userid",userid));
            if(courseStudentService.selectCount(new EntityWrapper<CourseStudent>().eq("studentid",studentInfo.getId()))==0){
                userService.deleteById(userid);
                studentInfoService.deleteById(studentInfo.getId());
                return R.ok("删除成功");
            }else{
                return R.error("该学生加入过课程或者参加过考试，不能删除");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("程序异常");
        }
    }

    /**
     * @author Wqd
     * @since 2019-4-10
     */
    @ApiOperation("根据班级查找所有学生")
    @PostMapping("/findStudentsByClassid/{classid}")
    public R findStudentsByClassid(@PathVariable Integer classid) {
        List<StudentInfo> studentInfoList = studentInfoService.selectList(new EntityWrapper<StudentInfo>().eq("classid",classid));
        for(StudentInfo studentInfo : studentInfoList){
            User user = userService.selectById(studentInfo.getUserid());
            studentInfo.setUser(user);
        }
        return R.ok("查询成功").put("data",studentInfoList);
    }

}

