package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.modules.sys.service.ComposePaperService;
import com.hznu.coursemanagerbackend.modules.sys.service.ExamService;
import com.hznu.coursemanagerbackend.modules.sys.service.ExampaperService;
import com.hznu.coursemanagerbackend.myBeans.R;
import com.hznu.coursemanagerbackend.modules.sys.entity.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/exampaper")
@Api(description = "试卷表接口", value = "ExamPaper")
public class ExampaperController extends AbstractController{

    @Autowired
    ExampaperService examPaperService;

    @Autowired
    ComposePaperService composePaperService;

    @Autowired
    ExamService examService;

    /**
     * @author Wqd
     * @since 2018-12-10
     */
    @PostMapping("/add")
    @ApiOperation("添加试卷")
    public R addExamPaper(@RequestBody Exampaper examPaper) {
        try {
            if (examPaperService.insert(examPaper)) {
                return R.ok("试卷记录生成成功!");
            } else {
                return R.error("试卷记录生成失败!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.error();
        }
    }

    /**
     * @author Wqd
     * @since 2018-12-11
     */
    @ApiOperation("按照条件查找试卷分页")
    @PostMapping("/list")
    public R list(@RequestParam Map<String,Object> params){
        PageUtils page = examPaperService.queryPage(params);
        return R.ok().put("page",page);
    }


    /**
     * @author: DragonDoor
     * @Date: 2018/12/26 17:03
     * @Description:
     */
    @ApiOperation("更新试卷信息")
    @PostMapping("/change/{teacherid}")
    public R modify(@RequestBody Exampaper examPaper) {
        examPaperService.updateById(examPaper);
        return R.ok();
    }

    /**
     * @author: DragonDoor
     * @Date: 2018/12/19 13:28
     * @Description: 
     */
    @ApiOperation("根据课程查找试卷")
    @PostMapping("/findByCourse")
    public R findByCourse(@RequestParam Map<String,Object> params)
    {
        return R.ok().put("page",examPaperService.findByCourse(params));
    }

    
    /**
     * @author: DragonDoor
     * @Date: 2018/12/19 13:28
     * @Description: 
     */
    @ApiOperation("根据id查找")
    @GetMapping("/findByid/{id}")
    public R findById(@PathVariable Integer id)
    {
        return R.ok().put("exampaper",examPaperService.selectById(id));
    }

    /**
     * @author: Wqd
     * @Date: 2018/12/21
     * @Description:
     */
    @ApiOperation("根据课程id查找所有已组完试卷")
    @PostMapping("/findAllByCourseId/{courseid}")
    public R findAllByCourseId(@PathVariable Integer courseid){
        List<Exampaper> exampaperList = examPaperService.selectList(new EntityWrapper<Exampaper>().eq("courseid",courseid));
        List<Exampaper> exampaperList1 = new LinkedList<>();
        for(Exampaper exampaper : exampaperList){
            Float points=(float)0;
            List<ComposePaper> composePaperList = composePaperService.selectList(new EntityWrapper<ComposePaper>().eq("paperid",exampaper.getId()));
            for(ComposePaper composePaper : composePaperList){
                points=points+composePaper.getPoints();
            }
            if(Math.abs(exampaper.getTotalpoints()-points)<0.0001){
                exampaperList1.add(exampaper);
            }
        }
        return R.ok().put("exampaper",exampaperList1);
    }

    /**
     * @author Wqd
     * @since 2019-3-29
     */
    @ApiOperation("删除试卷")
    @GetMapping("/removeExamPaper/{id}")
    public R removeExamPaper(@PathVariable Integer id) {
        try {
            if(examService.selectCount(new EntityWrapper<Exam>().eq("exampaperid",id))==0){
                return R.ok("删除成功").put("data", examPaperService.deleteById(id));
            }else{
                return R.error("该试卷已在考试中使用过，不能删除");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("程序异常");
        }
    }

}

