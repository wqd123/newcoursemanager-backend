package com.hznu.coursemanagerbackend.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.hznu.coursemanagerbackend.common.utils.MapUtils;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.utils.Query;
import com.hznu.coursemanagerbackend.modules.sys.entity.*;
import com.hznu.coursemanagerbackend.modules.sys.dao.CourseStudentDao;
import com.hznu.coursemanagerbackend.modules.sys.entity.Class;
import com.hznu.coursemanagerbackend.modules.sys.service.*;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.hznu.coursemanagerbackend.myBeans.R;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@Service
public class CourseStudentServiceImpl extends ServiceImpl<CourseStudentDao, CourseStudent> implements CourseStudentService {

    @Autowired
    StudentInfoService studentInfoService;
    @Autowired
    UserService userService;
    @Autowired
    ClassService classService;
    @Autowired
    CourseService courseService;
    @Autowired
    CourseTeachService courseTeachService;
    @Autowired
    TeacherInfoService teacherInfoService;
    @Autowired
    DepartmentService departmentService;

    @Override
    public PageUtils queryPageByCourseteachid(Map<String,Object> params)
    {
        CourseStudent courseStudent = new CourseStudent();
        try
        {
            BeanUtils.populate(courseStudent,params);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        String keyword = (String)params.get("keyword");
        Page<CourseStudent> page = new Query<CourseStudent>(params).getPage();
        List<CourseStudent> courseStudents = this.baseMapper.findByCondition(page,courseStudent,keyword);
        for(CourseStudent courseStudent1:courseStudents)
        {
            if(courseStudent.getCourseteachid()!=null)
            {
                StudentInfo studentInfo = studentInfoService.selectById(courseStudent1.getStudentid());
                studentInfo.setUser(userService.selectById(studentInfo.getUserid()));
                studentInfo.setClazz(classService.selectById(studentInfo.getClassid()));
                courseStudent1.setStudentInfo(studentInfo);
            }
            if(courseStudent.getStudentid()!=null)
            {
                CourseTeach courseTeach = courseTeachService.selectById(courseStudent1.getCourseteachid());
                courseTeach.setCourse(courseService.selectById(courseTeach.getCourseid()));
                TeacherInfo teacherInfo = teacherInfoService.selectById(courseTeach.getTeacherid());
                teacherInfo.setDepartment(departmentService.selectById(teacherInfo.getDepartmentid()));
                teacherInfo.setUser(userService.selectById(teacherInfo.getUserid()));
                courseTeach.setTeacher(teacherInfo);
                courseStudent1.setCourseTeach(courseTeach);
            }
        }
        page.setRecords(courseStudents);
        return new PageUtils(page);
    }

    @Override
    public Integer LoadBatch(List<Object> objectList, Integer courseTeachid){
        Integer count=0;
        try{
            for (Object object : objectList){
                Map<String, Object> map= MapUtils.getKeyAndValue(object);
                CourseStudent courseStudent=new CourseStudent();
                courseStudent.setCourseteachid(courseTeachid);
                StudentInfo studentInfo=new StudentInfo();
                if(map.get("workId")!=null) {
                    User user = userService.selectOne(new EntityWrapper<User>().eq("workid", map.get("workId")));
                    studentInfo = studentInfoService.selectOne(new EntityWrapper<StudentInfo>().eq("userid", user.getId()));
                    if (studentInfo == null) {
                        continue;
                    }
                }
                else{
                    continue;
                }
                courseStudent.setStudentid(studentInfo.getId());
                if (this.selectCount(new EntityWrapper<CourseStudent>().eq("courseteachid", courseTeachid).eq("studentid", studentInfo.getId())) > 0) {
                    continue;
                }
                this.insert(courseStudent);
                count++;
            }
            return count;
        }catch(Exception e){
        e.printStackTrace();
        return count;
        }
    }

}
