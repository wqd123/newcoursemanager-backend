package com.hznu.coursemanagerbackend.modules.sys.bean;

import com.hznu.coursemanagerbackend.modules.sys.entity.Exam;

public class ExamDetail extends Exam{
    private Integer attendExamid;

    public Integer getAttendExamid() {
        return attendExamid;
    }

    public void setAttendExamid(Integer attendExamid) {
        this.attendExamid = attendExamid;
    }
}
