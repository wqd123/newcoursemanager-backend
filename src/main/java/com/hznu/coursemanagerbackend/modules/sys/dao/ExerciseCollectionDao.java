package com.hznu.coursemanagerbackend.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseCollection;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DragonDoor
 * @since 2019-03-01
 */
public interface ExerciseCollectionDao extends BaseMapper<ExerciseCollection> {

}
