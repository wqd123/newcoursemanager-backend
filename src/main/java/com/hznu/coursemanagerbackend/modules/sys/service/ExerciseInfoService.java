package com.hznu.coursemanagerbackend.modules.sys.service;

import com.hznu.coursemanagerbackend.modules.sys.bean.ExerciseDetail;
import com.hznu.coursemanagerbackend.modules.sys.entity.ExerciseInfo;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public interface ExerciseInfoService extends IService<ExerciseInfo> {
     Integer LoadBatch(List<Object> objectList,Integer courseId);

     List<ExerciseDetail> findByCondition(ExerciseDetail exerciseDetail);

}
