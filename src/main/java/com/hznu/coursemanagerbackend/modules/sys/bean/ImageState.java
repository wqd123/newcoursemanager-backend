package com.hznu.coursemanagerbackend.modules.sys.bean;

/**
 * @Author: TateBrown
 * @date: 2018/12/10 21:51
 * @desc:图片状态
 * @return:
 */
public class ImageState {
    /**
     * 上传状态 上传成功时必须返回"SUCCESS"，失败时可以返回错误提示
     */
    private String state;

    /**
     * 上传的原文件名
     */
    private String original;

    /**
     * 文件大小
     */
    private String size;

    /**
     * 上传后的新文件名
     */
    private String title;

    /**
     * 文件类型
     */
    private String type;

    /**
     * 图片访问地址
     */
    private String url;

    public void setType(String type) {
        this.type = type;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public String getOriginal() {
        return original;
    }

    public String getSize() {
        return size;
    }

    public String getState() {
        return state;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
