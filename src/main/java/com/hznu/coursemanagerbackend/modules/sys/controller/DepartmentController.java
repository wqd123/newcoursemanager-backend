package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.common.utils.PageUtils;
import com.hznu.coursemanagerbackend.common.validator.ValidatorUtils;
import com.hznu.coursemanagerbackend.common.validator.group.AddGroup;
import com.hznu.coursemanagerbackend.modules.sys.entity.Department;
import com.hznu.coursemanagerbackend.modules.sys.entity.Major;
import com.hznu.coursemanagerbackend.modules.sys.entity.TeacherInfo;
import com.hznu.coursemanagerbackend.modules.sys.service.DepartmentService;
import com.hznu.coursemanagerbackend.modules.sys.service.MajorService;
import com.hznu.coursemanagerbackend.modules.sys.service.SchoolService;
import com.hznu.coursemanagerbackend.modules.sys.service.TeacherInfoService;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/department")
@Api(description = "系接口",value = "department")
public class DepartmentController {

    @Autowired
    DepartmentService departmentService;

    @Autowired
    SchoolService schoolService;

    @Autowired
    TeacherInfoService teacherInfoService;

    @Autowired
    MajorService majorService;

    /**
     * @author Wqd
     * @since 2018-11-28
     */
    @PostMapping("/add")
    @ApiOperation("添加系")
    public R add(@RequestBody Department department){
        try{
            Department isExist = new Department();
            isExist.setName(department.getName());
            //isExist.setSchoolid(department.getSchoolid());
            if(departmentService.findByName(isExist).size()!=0){
                return R.error("此系已存在");
            }else {
                department.setUniversityid(schoolService.findById(department.getSchoolid()).getUniversityid());
                departmentService.insert(department);
                return R.ok();
            }
        }catch (Exception e){
            e.printStackTrace();
            return R.error();
        }
    }

    /**
     * @author Wqd
     * @since 2018-12-2
     */
    @ApiOperation("根据学院id查找该学院的所有系部")
    @GetMapping("/finddepartmentbyschoolid/{schoolid}")
    public R finddepartmentbyschoolid(@PathVariable Integer schoolid) {
        try {
            return R.ok("获取成功").put("data", departmentService.findBySchoolid(schoolid));
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("程序异常");
        }
    }

    @ApiOperation("根据教师id查找该学院的所有系部")
    @PostMapping("/finddepartmentbyteacherid/{teacherid}")
    public R finddepartmentbyteacherid(@PathVariable Integer teacherid) {
        try {
            Integer schoolid = teacherInfoService.selectById(teacherid).getSchoolid();
            List<Department> list = departmentService.findBySchoolid(schoolid);
            return R.ok().put("data", list);
        } catch (Exception e) {
            e.printStackTrace();
            return R.error();
        }
    }

    /**
     * @author Wqd
     * @since 2019-3-28
     */
    @ApiOperation("删除系部")
    @GetMapping("/removedepartment/{id}")
    public R removedepartment(@PathVariable Integer id) {
        try {
            if(majorService.selectList(new EntityWrapper<Major>().eq("departmentid",id)).size()==0){
                return R.ok("删除成功").put("data", departmentService.deleteById(id));
            }else{
                return R.error("系部下有专业，不能删除");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("程序异常");
        }
    }

    /**
     * @author Wqd
     * @since 2019-3-31
     */
    @ApiOperation("条件查询系部（分页）")
    @PostMapping("/list")
    public R list(@RequestParam Map<String,Object> params)
    {
        PageUtils page = departmentService.queryPage(params);
        return R.ok().put("page",page);
    }

    @ApiOperation("根据学院id筛选系（分页）")
    @PostMapping("/findDepartmentBySchoolId")
    public R findDepartmentBySchoolId(@RequestParam Map<String,Object> params)
    {
        PageUtils page = departmentService.queryPageBySchoolId(params);
        return R.ok().put("page",page);
    }

    @ApiOperation("修改系部信息")
    @PostMapping("/modify")
    public R modify(@RequestBody Department department) {
        ValidatorUtils.validateEntity(department, AddGroup.class);
        departmentService.updateById(department);
        return R.ok();
    }

}

