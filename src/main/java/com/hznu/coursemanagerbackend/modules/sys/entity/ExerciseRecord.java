package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class ExerciseRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer answerrecordid;
    private String answer;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAnswerrecordid() {
        return answerrecordid;
    }

    public void setAnswerrecordid(Integer answerrecordid) {
        this.answerrecordid = answerrecordid;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "ExerciseRecord{" +
        ", id=" + id +
        ", answerrecordid=" + answerrecordid +
        ", answer=" + answer +
        "}";
    }
}
