package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 学校id
     */
    private Integer universityid;
    /**
     * 学院id
     */
    private Integer schoolid;
    /**
     * 用户名
     */
    private String username;
    private String password;
    /**
     * 加盐
     */
    private String salt;
    /**
     * 用户号
     */
    private String workid;
    private Integer sex;
    private Integer usertype;
    private String realname;
    /**
     * 以下是附加字段
     * newPassword:新密码
     * checkPassword:确认密码
     * captchaCode:验证码
     */
    @TableField(exist = false)
    private String newPassword;
    @TableField(exist = false)
    private String checkPassword;

    //附加字段
    @TableField(exist = false)
    private StudentInfo studentInfo;
    @TableField(exist = false)
    private TeacherInfo teacherInfo;


    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public StudentInfo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(StudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }

    public TeacherInfo getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(TeacherInfo teacherInfo) {
        this.teacherInfo = teacherInfo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUniversityid() {
        return universityid;
    }

    public void setUniversityid(Integer universityid) {
        this.universityid = universityid;
    }

    public Integer getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Integer schoolid) {
        this.schoolid = schoolid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getWorkid() {
        return workid;
    }

    public void setWorkid(String workid) {
        this.workid = workid;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getUsertype() {
        return usertype;
    }

    public void setUsertype(Integer usertype) {
        this.usertype = usertype;
    }

    public String getCheckPassword() {
        return checkPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setCheckPassword(String checkPassword) {
        this.checkPassword = checkPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public String toString() {
        return "User{" +
        ", id=" + id +
        ", universityid=" + universityid +
        ", schoolid=" + schoolid +
        ", username=" + username +
        ", password=" + password +
        ", salt=" + salt +
        ", workid=" + workid +
        ", sex=" + sex +
        ", usertype=" + usertype +
        "}";
    }
}
