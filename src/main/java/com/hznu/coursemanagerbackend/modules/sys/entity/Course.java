package com.hznu.coursemanagerbackend.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private Integer creatorid;
    private Integer maintainerid;


    /**
     * 附加字段
     *
     * @return
     */
    @TableField(exist = false)
    private String creator;  //创建者详情
    @TableField(exist = false)
    private String maintainer;   //  维护者详情


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCreatorid() {
        return creatorid;
    }

    public void setCreatorid(Integer creatorid) {
        this.creatorid = creatorid;
    }

    public Integer getMaintainerid() {
        return maintainerid;
    }

    public void setMaintainerid(Integer maintainerid) {
        this.maintainerid = maintainerid;
    }

    public String getCreator() {
        return creator;
    }

    public String getMaintainer() {
        return maintainer;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setMaintainer(String maintainer) {
        this.maintainer = maintainer;
    }

    @Override
    public String toString() {
        return "Course{" +
        ", id=" + id +
        ", name=" + name +
        ", creatorid=" + creatorid +
        ", maintainerid=" + maintainerid +
        "}";
    }
}
