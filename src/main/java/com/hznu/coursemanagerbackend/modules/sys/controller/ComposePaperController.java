package com.hznu.coursemanagerbackend.modules.sys.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.hznu.coursemanagerbackend.modules.sys.entity.ComposePaper;
import com.hznu.coursemanagerbackend.modules.sys.service.ComposePaperService;
import com.hznu.coursemanagerbackend.myBeans.R;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DragonDoor
 * @since 2018-11-12
 */
@EnableAutoConfiguration //自动载入应用程序所需的所有Bean
@RestController //@Controller与@ResponseBody的合并
@RequestMapping("/composePaper")
public class ComposePaperController {

    @Autowired
    ComposePaperService composePaperService;
    /**
     * @author Wqd
     * @since 2018-12-11
     */
    @ApiOperation("添加题目")
    @PostMapping("/add")
    public R add(@RequestBody ComposePaper composePaper) throws Exception{
        try{
            if(composePaperService.selectOne(
                    new EntityWrapper<ComposePaper>().eq("paperid",composePaper.getPaperid()).and().
                            eq("exerinfoid",composePaper.getExerinfoid()))==null){
                composePaperService.insert(composePaper);
                return R.ok("添加成功");
            }
            else{
                return R.error("已存在");
            }
        }catch(Exception e){
            e.printStackTrace();
            return R.error("添加失败");
        }
    }
    /**
     * @author: DragonDoor
     * @Date: 2018/12/17 12:00
     * @Description:
     */
    @ApiOperation("通过试卷id查找所有题目")
    @PostMapping("/findByPaperid/{paperid}")
    public R findByPaperid(@PathVariable Integer paperid) {

        return R.ok().put("data", composePaperService.findByPaperid(paperid));
    }

    /**
     * @author: DragonDoor
     * @Date: 2018/12/17 15:41
     * @Description: 
     */
    @ApiOperation("添加一题，再加入试卷中")
    @PostMapping("/addExer")
    public R addExerandCompose(@RequestBody ComposePaper composePaper)
    {
        composePaperService.addExerandCompose(composePaper);
        return R.ok();
    }

    /**
     * @author: DragonDoor
     * @Date: 2018/12/17 15:55
     * @Description: 
     */
    @ApiOperation("通过composePaperid删除题目")
    @GetMapping("/remove/{composePaperid}")
    public R removeOne(@PathVariable Integer composePaperid){
        try{
            composePaperService.deleteById(composePaperid);
            return R.ok();
        }catch (Exception e){
            e.printStackTrace();
            return R.error(e.getMessage());
        }
    }

    /**
     * @author: Wqd
     * @Date: 2019/3/10
     * @Description:
     */
    @ApiOperation("更新题目分值")
    @PostMapping("/change")
    public R modify(@RequestBody ComposePaper composePaper){
        ComposePaper composePaper1 = composePaperService.selectOne(new EntityWrapper<ComposePaper>().eq("paperid",composePaper.getPaperid()).eq("exerinfoid",composePaper.getExerinfoid()));
        composePaper1.setPoints(composePaper.getPoints());
        composePaperService.updateById(composePaper1);
        return R.ok("修改成功！");
    }

}

