package com.hznu.coursemanagerbackend.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 上传路径配置
 *
 * @author wheelchen
 */
@ConfigurationProperties("storage")
@Component
public class StorageConfig {

    /**
     * Folder location for storing files
     */
    private String location = "upload-dir";

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
