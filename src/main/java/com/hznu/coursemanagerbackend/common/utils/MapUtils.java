/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hznu.coursemanagerbackend.common.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;


/**
 * Map工具类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 2.0.0
 */
public class MapUtils extends HashMap<String, Object> {

    @Override
    public MapUtils put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    /**
     * @Description:根据Object对象反射成Map
     * @param: object
     * @return:
     * @author: TateBrown
     * @Date: 2018/12/10
     */
    public static Map<String,Object> getKeyAndValue(Object obj){
        Map<String,Object> map=new HashMap<String,Object>();
        Class dto=(Class) obj.getClass();
        //获取类中所有属性
        Field[] fs=dto.getDeclaredFields();
        for(int i=0;i<fs.length;i++){
            Field f=fs[i];
            f.setAccessible(true);//设置访问
            Object val=new Object();
            try{
            val=f.get(obj);
            map.put(f.getName(),val);
            }catch(IllegalArgumentException e){
              e.printStackTrace();
            }catch (IllegalAccessException e){
                e.printStackTrace();
            }
        }
        return map;
    }
}
