package com.hznu.coursemanagerbackend.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by TAO on 2015/11/20.
 * 公共工具类
 *
 * 功能列表：
 * 把字节数组转换成16进制字符串——bytesToHexString
 * Convert hex string to byte[]——hexStringToBytes
 * Convert char to byte——charToByte
 * 生成短信验证码——getSmsCode
 * 判断字符串是否为整型数字——isIntNumeric
 * IDList字符串形式转换为List<Long>——getLongIdList
 * IDList字符串形式转换为List<Integer>——getIntIdList
 * IDList字符串形式转换为List<String>——getStringList
 * 获取UUID——getUUID
 *
 */
public class PublicUtils {


    // 获取UUID
    public static String getUUID() {
        UUID uuid = UUID.randomUUID();
        String str = uuid.toString();
        // 去掉"-"符号
        //String temp = str.substring(0, 8) + str.substring(9, 13) + str.substring(14, 18) + str.substring(19, 23) + str.substring(24);
        return str;
    }

    //判断字符串是否为整型数字
    public static boolean isIntNumeric(String str){
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if( !isNum.matches() ){
            return false;
        }
        return true;
    }

    //IDList字符串形式转换为List<Long>
    public static  List<Long> getLongIdList(String idStr, String splitCahr) {
        String[] sourceStrArray = idStr.split(splitCahr);
        List<Long> idList = new ArrayList<Long>();
        for (int i = 0; i < sourceStrArray.length; i++) {
            idList.add(Long.parseLong(sourceStrArray[i]));
        }
        return idList;
    }
    //IDList字符串形式转换为List<Integer>
    public static List<Integer> getIntIdList(String idStr, String splitCahr) {
        String[] sourceStrArray = idStr.split(splitCahr);
        List<Integer> idList = new ArrayList<Integer>();
        for (int i = 0; i < sourceStrArray.length; i++) {
            idList.add(Integer.parseInt(sourceStrArray[i]));
        }
        return idList;
    }
    //IDList字符串形式转换为List<String>
    public static List<String> getStringList(String nameStr, String splitCahr) {
        String[] sourceStrArray = nameStr.split(splitCahr);
        List<String> nameList = new ArrayList<String>();
        for (int i = 0; i < sourceStrArray.length; i++) {
            nameList.add(String.valueOf(sourceStrArray[i]));
        }
        return nameList;
    }

    //生成短信验证码
    public static String getSmsCode()
    {
        Random random = new Random();
        String code="";
        int n;
        while (true){
            n=random.nextInt(10);
//                if(code.contains(String.valueOf(n))) //控制验证码不出现重复数字
//                    continue;
            code+=n;
            if(code.length()==6  ) //验证码6位
                break;
        }
        return code;
    }

    /**
     * 把字节数组转换成16进制字符串
     *
     * @param buffer
     * @return
     */
    public static String bytesToHexString(byte[] buffer) {
        StringBuffer sb = new StringBuffer(buffer.length * 2);
        for (int i = 0; i < buffer.length; i++) {
            sb.append(Character.forDigit((buffer[i] & 240) >> 4, 16));
            sb.append(Character.forDigit(buffer[i] & 15, 16));
        }
        return sb.toString();
    }

    /**
     * Convert hex string to byte[]
     *
     * @param hexString the hex string
     * @return byte[]
     */
    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    /**
     * Convert char to byte
     *
     * @param c char
     * @return byte
     */
    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    //java List转换为字符串并加入分隔符
    public static String listToString(List list, char separator) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i)).append(separator);
        }
        return sb.toString().substring(0, sb.toString().length() - 1);
    }

}
