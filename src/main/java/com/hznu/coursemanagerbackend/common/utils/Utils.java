package com.hznu.coursemanagerbackend.common.utils;

import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Random;

/**
 * Created by TAO on 2015/11/20.
 */
public class Utils {

    /**
     * 把字节数组转换成16进制字符串
     *
     * @param buffer
     * @return
     */
    public static String bytesToHexString(byte[] buffer) {
        StringBuffer sb = new StringBuffer(buffer.length * 2);
        for (int i = 0; i < buffer.length; i++) {
            sb.append(Character.forDigit((buffer[i] & 240) >> 4, 16));
            sb.append(Character.forDigit(buffer[i] & 15, 16));
        }
        return sb.toString();
    }

    /**
     * Convert hex string to byte[]
     *
     * @param hexString the hex string
     * @return byte[]
     */
    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    /**
     * Convert char to byte
     *
     * @param c char
     * @return byte
     */
    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    /**
     * 字符串转换成十六进制字符串
     *
     * @param str 待转换的ASCII字符串
     * @return String 每个Byte之间空格分隔，如: [61 6C 6B]
     */
    public static String str2HexStr(String str) {

        char[] chars = "0123456789ABCDEF".toCharArray();
        StringBuilder sb = new StringBuilder("");
        byte[] bs = str.getBytes();
        int bit;

        for (int i = 0; i < bs.length; i++) {
            bit = (bs[i] & 0x0f0) >> 4;
            sb.append(chars[bit]);
            bit = bs[i] & 0x0f;
            sb.append(chars[bit]);
        }
        return sb.toString().trim();
    }

    /**
     * 十六进制转换字符串
     *
     * @param hexStr Byte字符串(Byte之间无分隔符 如:[616C6B])
     * @return String 对应的字符串
     */
    public static String hexStr2Str(String hexStr) {
        String str = "0123456789ABCDEF";
        char[] hexs = hexStr.toCharArray();
        byte[] bytes = new byte[hexStr.length() / 2];
        int n;

        for (int i = 0; i < bytes.length; i++) {
            n = str.indexOf(hexs[2 * i]) * 16;
            n += str.indexOf(hexs[2 * i + 1]);
            bytes[i] = (byte) (n & 0xff);
        }
        return new String(bytes);
    }

    /**
     * author: WheelChen
     * date: 2017/7/19
     * function: 获取图片文件格式
     *
     * @param path
     * @return 图片格式
     */
    public static String getExt(String path) {
        String ext;
        if (StringUtils.isEmpty(path)) ext = "";
        else if (path.lastIndexOf(".") == -1) ext = "";
        else ext = path.substring(path.lastIndexOf(".") + 1);
        return ext;
    }



    /**
     * 获取16进制随机数
     * @param len 长度
     * @return
     */
    public static String randomHexString(int len)  {
        try {
            StringBuffer result = new StringBuffer();
            for(int i=0;i<len;i++) {
                result.append(Integer.toHexString(new Random().nextInt(16)));
            }
            return result.toString().toUpperCase();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     * author: WheelChen
     * date: 2017/8/7
     * function: 随机生成byte[]
     *
     * @param len 长度
     * @return
     */
    public static byte[] randomBytes(int len){
        //生成两倍的16进制字符串
        String hexStr = randomHexString(len * 2);
        //转byte[]
        return hexStringToBytes(hexStr);
    }

    public static String getWebRootURL(Object o){
        String clazzPath = o.getClass().getResource("").getPath();
        int i = clazzPath.lastIndexOf("ROOT");
        String savePath = clazzPath.substring(0, i-1); // 图片文件夹digitu_pic直接放在webapps下

        return savePath;
    }


    /**
     * 型如："data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAgAAA..."
     * 取第一个'/' 与 'base64' 之间的
     * @param base64
     * @return
     */
    public static String getBase64StringExt(String base64) throws IOException {
//        if (!Base64.isBase64(base64))
//            throw new IOException("不是Base64的字符串");

        return  base64.substring(base64.indexOf("/") + 1, base64.indexOf("base64") - 1);
    }

    public static String getBase64StringData(String base64) {
        return base64.substring(base64.indexOf(",") + 1);
    }
    /**
     * @Description:给字符串前后添加合法的Html标签
     * @param: s,tag
     * @return:
     * @author: TateBrown
     * @Date: 2018/12/10
     */
    public static String AddTag(String s,String tag){
        return "<"+tag+">"+s+"</"+tag+">";
    }
}
