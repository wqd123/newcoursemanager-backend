package com.hznu.coursemanagerbackend.common.ueditor.define;

/**
 * @Author: TateBrown
 * @date: 2018/12/31 12:49
 * @param:
 * @return:
 */
public interface State {
    public boolean isSuccess();

    public void putInfo(String name, String val);

    public void putInfo(String name, long val);

    public String toJSONString();
}
