package com.hznu.coursemanagerbackend.common.ueditor.define;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: TateBrown
 * @date: 2018/12/31 12:51
 * @param:
 * @return:
 */
public class MIMEType {
    public static final Map<String, String> types = new HashMap<String, String>() {{
        put("image/gif", ".gif");
        put("image/jpeg", ".jpg");
        put("image/jpg", ".jpg");
        put("image/png", ".png");
        put("image/bmp", ".bmp");
    }};

    public static String getSuffix(String mime) {
        return MIMEType.types.get(mime);
    }
}
