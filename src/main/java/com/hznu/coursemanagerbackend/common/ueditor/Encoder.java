package com.hznu.coursemanagerbackend.common.ueditor;

/**
 * @Author: TateBrown
 * @date: 2018/12/31 13:07
 * @param:
 * @return:
 */
public class Encoder {
    public static String toUnicode(String input) {

        StringBuilder builder = new StringBuilder();
        char[] chars = input.toCharArray();

        for (char ch : chars) {

            if (ch < 256) {
                builder.append(ch);
            } else {
                builder.append("\\u" + Integer.toHexString(ch & 0xffff));
            }

        }

        return builder.toString();

    }
}
