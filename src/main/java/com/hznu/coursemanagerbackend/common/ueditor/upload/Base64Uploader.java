package com.hznu.coursemanagerbackend.common.ueditor.upload;

import com.hznu.coursemanagerbackend.common.ueditor.PathFormat;
import com.hznu.coursemanagerbackend.common.ueditor.define.AppInfo;
import com.hznu.coursemanagerbackend.common.ueditor.define.BaseState;
import com.hznu.coursemanagerbackend.common.ueditor.define.FileType;
import com.hznu.coursemanagerbackend.common.ueditor.define.State;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

import java.util.Map;

/**
 * @Author: TateBrown
 * @date: 2018/12/31 12:59
 * @param:
 * @return:
 */
public final class Base64Uploader {
    public static State save(String content, Map<String, Object> conf) {

        byte[] data = decode(content);

        long maxSize = ((Long) conf.get("maxSize")).longValue();

        if (!validSize(data, maxSize)) {
            return new BaseState(false, AppInfo.MAX_SIZE);
        }

        String suffix = FileType.getSuffix("JPG");

        String savePath = PathFormat.parse((String) conf.get("savePath"),
                (String) conf.get("filename"));
        String localSavePathPrefix = PathFormat.parse((String) conf.get("localSavePathPrefix"),
                (String) conf.get("filename"));
        savePath = savePath + suffix;
        localSavePathPrefix = localSavePathPrefix + suffix;
        String physicalPath = localSavePathPrefix;
        State storageState = StorageManager.saveBinaryFile(data, physicalPath);

        if (storageState.isSuccess()) {
            storageState.putInfo("url", PathFormat.format(savePath));
            storageState.putInfo("type", suffix);
            storageState.putInfo("original", "");
        }

        return storageState;
    }

    private static byte[] decode(String content) {
        return Base64.decodeBase64(StringUtils.getBytesUtf8(content));
    }

    private static boolean validSize(byte[] data, long length) {
        return data.length <= length;
    }

}
