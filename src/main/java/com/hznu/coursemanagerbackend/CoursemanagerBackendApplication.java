package com.hznu.coursemanagerbackend;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@SpringBootApplication  //整个boot的核心注解，目的是开启boot的自动配置
@EnableTransactionManagement //启动注解事务管理，等同于xml配置方式<tx:annotation-driven/>
@MapperScan("com.hznu.coursemanagerbackend.modules.sys.dao") //扫描mybatis的mapper接口位于com.project.dao包下
@EnableCaching //启用缓存
@EnableSwagger2
@EnableConfigurationProperties
public class CoursemanagerBackendApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        System.out.println("Application start run......");
        SpringApplication application = new SpringApplication(CoursemanagerBackendApplication.class);
        application.run(args);
        System.out.println("Application run finished!");
    }
}
