package com.hznu.coursemanagerbackend.myBeans;

import com.baomidou.mybatisplus.annotations.TableField;

/**
 * @author: DragonDoor
 * @Date: 2018/12/3 13:48
 * @Description: 构建树
 */
public class TreeSupport {

    @TableField(exist = false)
    private String treeKey;//树关键词
    @TableField(exist = false)
    private String parentId;//树父节点

    public String getTreeKey() {
        return treeKey;
    }

    public void setTreeKey(String treeKey) {
        this.treeKey = treeKey;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}
