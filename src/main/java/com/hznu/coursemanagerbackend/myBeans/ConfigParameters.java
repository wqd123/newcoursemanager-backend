package com.hznu.coursemanagerbackend.myBeans;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 自动映射一个POJO到Spring Boot配置文件（默认是application.yml文件）的属性集。
 * config配置在application.yml文件中,会按相同名称自动注入到Bean中
 *
 */
@Component //必须加上，才能自动注入
@ConfigurationProperties(prefix="config")
public class ConfigParameters {

    private String jwtSecret; //token密钥
    private int jwtExpire; //token有效时时，单位：秒

    private  String aliDaYuServer;  //阿里大鱼服务器IP
    private  String aliDaYuAppKey;  //阿里大鱼app key
    private  String aliDaYuSecret;  //阿里大鱼secret
    private  String aliDaYuSmsSignName; //
    private  String aliDaYuProduct; //
    private  int aliDaYuSmsExpireMinute;

//    private  String emailHost;  //smtp服务器
//    private  String emailUserName;  //smtp用户名
//    private  String emailPassword;  //smtp密码

    public String getDefaultPassword() {
        return defaultPassword;
    }

    public void setDefaultPassword(String defaultPassword) {
        this.defaultPassword = defaultPassword;
    }

    private String defaultPassword;

    public String getJwtSecret() {
        return jwtSecret;
    }

    public void setJwtSecret(String jwtSecret) {
        this.jwtSecret = jwtSecret;
    }

    public int getJwtExpire() {
        return jwtExpire;
    }

    public void setJwtExpire(int jwtExpire) {
        this.jwtExpire = jwtExpire;
    }

    public String getAliDaYuServer() {
        return aliDaYuServer;
    }

    public void setAliDaYuServer(String aliDaYuServer) {
        this.aliDaYuServer = aliDaYuServer;
    }

    public String getAliDaYuAppKey() {
        return aliDaYuAppKey;
    }

    public void setAliDaYuAppKey(String aliDaYuAppKey) {
        this.aliDaYuAppKey = aliDaYuAppKey;
    }

    public String getAliDaYuSecret() {
        return aliDaYuSecret;
    }

    public void setAliDaYuSecret(String aliDaYuSecret) {
        this.aliDaYuSecret = aliDaYuSecret;
    }

    public String getAliDaYuSmsSignName() {
        return aliDaYuSmsSignName;
    }

    public void setAliDaYuSmsSignName(String aliDaYuSmsSignName) {
        this.aliDaYuSmsSignName = aliDaYuSmsSignName;
    }

    public String getAliDaYuProduct() {
        return aliDaYuProduct;
    }

    public void setAliDaYuProduct(String aliDaYuProduct) {
        this.aliDaYuProduct = aliDaYuProduct;
    }

    public int getAliDaYuSmsExpireMinute() {
        return aliDaYuSmsExpireMinute;
    }

    public void setAliDaYuSmsExpireMinute(int aliDaYuSmsExpireMinute) {
        this.aliDaYuSmsExpireMinute = aliDaYuSmsExpireMinute;
    }

//    public String getEmailHost() {
//        return emailHost;
//    }
//
//    public void setEmailHost(String emailHost) {
//        this.emailHost = emailHost;
//    }
//
//    public String getEmailUserName() {
//        return emailUserName;
//    }
//
//    public void setEmailUserName(String emailUserName) {
//        this.emailUserName = emailUserName;
//    }
//
//    public String getEmailPassword() {
//        return emailPassword;
//    }
//
//    public void setEmailPassword(String emailPassword) {
//        this.emailPassword = emailPassword;
//    }
}

