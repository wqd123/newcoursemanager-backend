
/*
Navicat MySQL Data Transfer

Source Server         : weimeng
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : newcoursemanager

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2019-04-17 16:37:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for answer_record
-- ----------------------------
DROP TABLE IF EXISTS `answer_record`;
CREATE TABLE `answer_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `examid` int(11) DEFAULT NULL,
  `exerciseid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of answer_record
-- ----------------------------
INSERT INTO `answer_record` VALUES ('1', '3', null, '1');
INSERT INTO `answer_record` VALUES ('2', '3', '-1', '4');

-- ----------------------------
-- Table structure for attendexam
-- ----------------------------
DROP TABLE IF EXISTS `attendexam`;
CREATE TABLE `attendexam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `examid` int(11) DEFAULT NULL,
  `score` float DEFAULT NULL,
  `loginip` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `remain` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of attendexam
-- ----------------------------
INSERT INTO `attendexam` VALUES ('34', '4', '11', null, null, null, null, null);
INSERT INTO `attendexam` VALUES ('35', '3', '11', null, null, null, null, null);

-- ----------------------------
-- Table structure for class
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `majorid` int(11) DEFAULT NULL,
  `departmentid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `universityid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of class
-- ----------------------------
INSERT INTO `class` VALUES ('1', '计算机163', '1', '1', '1', '1');
INSERT INTO `class` VALUES ('2', '软工162', '2', '2', '1', '1');
INSERT INTO `class` VALUES ('3', '数学161', '3', '3', '2', '1');

-- ----------------------------
-- Table structure for compose_paper
-- ----------------------------
DROP TABLE IF EXISTS `compose_paper`;
CREATE TABLE `compose_paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paperid` int(11) DEFAULT NULL,
  `exerinfoid` int(11) DEFAULT NULL,
  `points` float DEFAULT NULL,
  `seq` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of compose_paper
-- ----------------------------
INSERT INTO `compose_paper` VALUES ('1', '1', '1', '20', null);
INSERT INTO `compose_paper` VALUES ('2', '1', '2', '50', null);
INSERT INTO `compose_paper` VALUES ('3', '3', '1', '10', null);
INSERT INTO `compose_paper` VALUES ('4', '3', '2', '10', null);
INSERT INTO `compose_paper` VALUES ('5', '3', '3', '10', null);
INSERT INTO `compose_paper` VALUES ('6', '3', '4', '10', null);
INSERT INTO `compose_paper` VALUES ('7', '3', '5', '20', null);
INSERT INTO `compose_paper` VALUES ('8', '3', '6', '10', null);
INSERT INTO `compose_paper` VALUES ('9', '3', '7', '40', null);

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `creatorid` int(11) DEFAULT NULL,
  `maintainerid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('8', 'Python', '2', '2');
INSERT INTO `course` VALUES ('9', 'JAVA程序设计', '2', '2');
INSERT INTO `course` VALUES ('10', 'C语言程序设计', '2', '2');
INSERT INTO `course` VALUES ('11', '网络信息安全', '2', '2');
INSERT INTO `course` VALUES ('12', '物联网概论', '2', '2');
INSERT INTO `course` VALUES ('13', 'python', '2', '2');

-- ----------------------------
-- Table structure for course_student
-- ----------------------------
DROP TABLE IF EXISTS `course_student`;
CREATE TABLE `course_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseteachid` int(11) DEFAULT NULL,
  `studentid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of course_student
-- ----------------------------
INSERT INTO `course_student` VALUES ('1', '1', '4');
INSERT INTO `course_student` VALUES ('2', '10', '7');
INSERT INTO `course_student` VALUES ('3', '1', '3');
INSERT INTO `course_student` VALUES ('4', '6', '1');
INSERT INTO `course_student` VALUES ('5', '6', '2');
INSERT INTO `course_student` VALUES ('6', '6', '3');
INSERT INTO `course_student` VALUES ('7', '6', '4');
INSERT INTO `course_student` VALUES ('8', '6', '5');
INSERT INTO `course_student` VALUES ('9', '6', '6');
INSERT INTO `course_student` VALUES ('10', '6', '7');
INSERT INTO `course_student` VALUES ('11', '6', '8');
INSERT INTO `course_student` VALUES ('12', '6', '9');

-- ----------------------------
-- Table structure for course_teach
-- ----------------------------
DROP TABLE IF EXISTS `course_teach`;
CREATE TABLE `course_teach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseid` int(11) DEFAULT NULL,
  `coursename` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `nickname` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `teacherid` int(11) DEFAULT NULL,
  `iscreator` int(11) DEFAULT NULL,
  `year` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `term` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of course_teach
-- ----------------------------
INSERT INTO `course_teach` VALUES ('1', '8', 'Python', 'java', '2', '2', '2018', '大三上', '勤园11-404');
INSERT INTO `course_teach` VALUES ('2', '9', 'JAVA程序设计', 'Java', '2', '2', '2018', '大三上', '勤园11-404');
INSERT INTO `course_teach` VALUES ('6', '10', 'C语言程序设计', 'C语言', '2', null, '2018', '大三上', '勤园11-404');
INSERT INTO `course_teach` VALUES ('7', '11', '网络信息安全', 'test', '3', null, null, null, null);
INSERT INTO `course_teach` VALUES ('9', '12', '物联网概论', '物联网', '2', '2', '2018', '大三上', '11-404');
INSERT INTO `course_teach` VALUES ('10', '8', 'Python', '111', '2', '2', '11', '11', '11');

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `universityid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('1', '计算机系', '1', '1', '1');
INSERT INTO `department` VALUES ('2', '软件工程系', '1', '1', null);
INSERT INTO `department` VALUES ('3', '数学系1', '2', '1', '3');

-- ----------------------------
-- Table structure for exam
-- ----------------------------
DROP TABLE IF EXISTS `exam`;
CREATE TABLE `exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `place` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `examdate` date DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `finishtime` datetime DEFAULT NULL,
  `exampaperid` int(11) DEFAULT NULL,
  `nop` int(11) DEFAULT NULL,
  `invigilator1` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `invigilator2` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of exam
-- ----------------------------
INSERT INTO `exam` VALUES ('11', '测试2', '11-404', null, '2019-02-28 17:18:18', '2019-04-01 17:18:20', '1', '11', '1', '11', '3', '40');
INSERT INTO `exam` VALUES ('12', '测试100', '11-101', null, '2019-04-02 19:12:38', '2019-04-03 19:12:50', '1', '11', '1', '11', '3', '40');

-- ----------------------------
-- Table structure for exampaper
-- ----------------------------
DROP TABLE IF EXISTS `exampaper`;
CREATE TABLE `exampaper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `noe` int(11) DEFAULT NULL,
  `totalpoints` float DEFAULT NULL,
  `dlevel` int(11) DEFAULT NULL,
  `noet` int(11) DEFAULT NULL,
  `courseid` int(11) DEFAULT NULL,
  `teacherid` int(11) DEFAULT NULL,
  `isshare` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of exampaper
-- ----------------------------
INSERT INTO `exampaper` VALUES ('1', '操作系统试卷', null, '100', null, null, '8', null, null);
INSERT INTO `exampaper` VALUES ('2', '操作系统试卷2', null, '80', null, null, '8', null, null);
INSERT INTO `exampaper` VALUES ('3', '测试1', null, '100', '1', null, '8', null, '2');

-- ----------------------------
-- Table structure for exam_courseteach
-- ----------------------------
DROP TABLE IF EXISTS `exam_courseteach`;
CREATE TABLE `exam_courseteach` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `examid` int(11) DEFAULT NULL,
  `courseteachid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of exam_courseteach
-- ----------------------------
INSERT INTO `exam_courseteach` VALUES ('22', '10', '1');
INSERT INTO `exam_courseteach` VALUES ('23', '11', '1');

-- ----------------------------
-- Table structure for exam_record
-- ----------------------------
DROP TABLE IF EXISTS `exam_record`;
CREATE TABLE `exam_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answerrecordid` int(11) DEFAULT NULL,
  `answer` text COLLATE utf8_bin,
  `istrue` int(11) DEFAULT NULL,
  `score` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of exam_record
-- ----------------------------

-- ----------------------------
-- Table structure for exercise_collection
-- ----------------------------
DROP TABLE IF EXISTS `exercise_collection`;
CREATE TABLE `exercise_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `studentid` int(11) DEFAULT NULL COMMENT '学生id',
  `courseid` int(11) DEFAULT NULL,
  `exerciseinfoid` int(11) DEFAULT NULL COMMENT '题目情况id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of exercise_collection
-- ----------------------------

-- ----------------------------
-- Table structure for exercise_info
-- ----------------------------
DROP TABLE IF EXISTS `exercise_info`;
CREATE TABLE `exercise_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creatorid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `dlevel` int(11) DEFAULT NULL,
  `courseid` int(11) DEFAULT NULL,
  `content` text COLLATE utf8_bin,
  `answer` text COLLATE utf8_bin,
  `comment` text COLLATE utf8_bin,
  `isready` int(11) DEFAULT NULL,
  `isremove` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of exercise_info
-- ----------------------------
INSERT INTO `exercise_info` VALUES ('1', '2', '1', '1', '8', 0x3C703EE69DADE5B79EE5B888E88C83E5A4A7E5ADA6E79A84E7AE80E7A7B0E698AFEFBC9A3C2F703E3C703E412E485A4E553C2F703E3C703E422E5A48485A4E553C2F703E, 0x410A, 0x3C703EE69CA8E69C893C2F703E, '1', '1');
INSERT INTO `exercise_info` VALUES ('2', '2', '1', '2', '8', 0x3C703EE8AFB7E997AE605C73717274207B32327D60E794A8E4BBA3E7A081E6808EE4B988E5AE9EE78EB03C2F703E3C703E412E73717274283232293C2F703E3C703E422E4D6174682E73717274283232293C2F703E, 0x420A, 0x3C703EE6A0B9E68DAE4A617661EFBC8C4D617468E5BA933C2F703E, '1', '1');
INSERT INTO `exercise_info` VALUES ('3', '2', '2', '1', '8', 0x3C703EE8BF99E698AFE4B880E4B8AAE5A49AE98089E9A2983C2F703E3C703E4131313C2F703E3C703E4231313C2F703E3C703E43323C2F703E, 0x0A5B2242222C2243225D, 0x3C703EE697A03C2F703E, '1', '1');
INSERT INTO `exercise_info` VALUES ('4', '2', '3', '1', '8', 0x3C703EE8BF99E698AFE4B880E4B8AAE588A4E696ADE9A2983C2F703E3C703E41E5AFB93C2F703E3C703E42E994993C2F703E, 0x31, 0x3C703EE697A03C2F703E, '1', '1');
INSERT INTO `exercise_info` VALUES ('5', '2', '4', '1', '8', 0x3C703EE8BF99E698AFE4B880E4B8AA5F5F5F5FE9A2983C2F703E, 0x3C703EE5A1ABE7A9BA3C2F703E, 0x3C703EE697A03C2F703E, '1', '1');
INSERT INTO `exercise_info` VALUES ('6', '2', '5', '1', '8', 0x3C703EE8AFB7E997AEE8BF99E698AFE4B880E4B8AAE4BB80E4B988E9A298EFBC9F3C2F703E3C703E3C62722F3E3C2F703E, 0x3C703EE7AE80E7AD94E9A2983C2F703E, 0x3C703EE697A03C2F703E, '1', '1');
INSERT INTO `exercise_info` VALUES ('7', '2', '6', '1', '8', 0x3C703EE8AFB7E8BE93E587BA68656C6C6F20776F726C643C2F703E, 0x3C70726520636C6173733D2262727573683A6370703B746F6F6C6261723A66616C7365223E7072696E7466282671756F743B68656C6C6F266E6273703B776F726C645C6E2671756F743B293B3C2F7072653E3C703E3C62722F3E3C2F703E3C703E3C62722F3E3C2F703E, 0x3C703EE697A03C2F703E, '1', '1');
INSERT INTO `exercise_info` VALUES ('8', '2', '2', '1', '8', 0x3C703EE8BF99E98193E9A298E69C89E587A0E4B8AAE98089E9A1B93C2F703E3C703E41313C2F703E3C703E42323C2F703E3C703E43E4BA8C3C2F703E, 0x5B2242222C2243225D, 0x3C703EE697A03C2F703E, '1', '1');
INSERT INTO `exercise_info` VALUES ('9', '2', '1', null, '8', null, null, null, '-1', '1');
INSERT INTO `exercise_info` VALUES ('10', '2', '1', null, '8', null, null, null, '-1', '1');
INSERT INTO `exercise_info` VALUES ('11', '2', '1', null, '8', null, 0x42, null, '1', '1');

-- ----------------------------
-- Table structure for exercise_record
-- ----------------------------
DROP TABLE IF EXISTS `exercise_record`;
CREATE TABLE `exercise_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answerrecordid` int(11) DEFAULT NULL,
  `answer` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of exercise_record
-- ----------------------------
INSERT INTO `exercise_record` VALUES ('1', '1', 0xE595A6E595A6E595A6);
INSERT INTO `exercise_record` VALUES ('2', '2', null);

-- ----------------------------
-- Table structure for exerk_point
-- ----------------------------
DROP TABLE IF EXISTS `exerk_point`;
CREATE TABLE `exerk_point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exerinfoid` int(11) DEFAULT NULL,
  `kpointid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of exerk_point
-- ----------------------------
INSERT INTO `exerk_point` VALUES ('3', '3', '2');
INSERT INTO `exerk_point` VALUES ('4', '4', '1');
INSERT INTO `exerk_point` VALUES ('5', '4', '2');
INSERT INTO `exerk_point` VALUES ('6', '4', '3');
INSERT INTO `exerk_point` VALUES ('7', '5', '1');
INSERT INTO `exerk_point` VALUES ('8', '5', '2');
INSERT INTO `exerk_point` VALUES ('9', '5', '3');
INSERT INTO `exerk_point` VALUES ('10', '6', '1');
INSERT INTO `exerk_point` VALUES ('11', '6', '2');
INSERT INTO `exerk_point` VALUES ('12', '6', '3');
INSERT INTO `exerk_point` VALUES ('19', '7', '1');
INSERT INTO `exerk_point` VALUES ('20', '7', '2');
INSERT INTO `exerk_point` VALUES ('21', '7', '3');
INSERT INTO `exerk_point` VALUES ('24', '2', '4');
INSERT INTO `exerk_point` VALUES ('25', '2', '5');
INSERT INTO `exerk_point` VALUES ('26', '8', '1');
INSERT INTO `exerk_point` VALUES ('27', '8', '2');
INSERT INTO `exerk_point` VALUES ('28', '8', '3');
INSERT INTO `exerk_point` VALUES ('29', '9', '1');
INSERT INTO `exerk_point` VALUES ('30', '9', '2');
INSERT INTO `exerk_point` VALUES ('31', '9', '3');
INSERT INTO `exerk_point` VALUES ('32', '10', '1');
INSERT INTO `exerk_point` VALUES ('33', '10', '2');
INSERT INTO `exerk_point` VALUES ('34', '10', '3');

-- ----------------------------
-- Table structure for kpoint
-- ----------------------------
DROP TABLE IF EXISTS `kpoint`;
CREATE TABLE `kpoint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `courseid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of kpoint
-- ----------------------------
INSERT INTO `kpoint` VALUES ('1', '简介', '-1', '8');
INSERT INTO `kpoint` VALUES ('2', '概括', '1', '8');
INSERT INTO `kpoint` VALUES ('3', '发展历史', '1', '8');
INSERT INTO `kpoint` VALUES ('4', '第一章', '-1', '8');
INSERT INTO `kpoint` VALUES ('5', '1.1基本操作', '4', '8');

-- ----------------------------
-- Table structure for major
-- ----------------------------
DROP TABLE IF EXISTS `major`;
CREATE TABLE `major` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `departmentid` int(11) DEFAULT NULL,
  `universityid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of major
-- ----------------------------
INSERT INTO `major` VALUES ('1', '计算机科学与技术（IT）', '1', '1', '1');
INSERT INTO `major` VALUES ('2', '软件工程', '1', '2', '1');
INSERT INTO `major` VALUES ('3', '数学研究1', '2', '3', '1');

-- ----------------------------
-- Table structure for paperexertype_info
-- ----------------------------
DROP TABLE IF EXISTS `paperexertype_info`;
CREATE TABLE `paperexertype_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exercisetype` int(11) DEFAULT NULL,
  `seq` int(11) DEFAULT NULL,
  `exampaperid` int(11) DEFAULT NULL,
  `exercisenum` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of paperexertype_info
-- ----------------------------

-- ----------------------------
-- Table structure for paper_share
-- ----------------------------
DROP TABLE IF EXISTS `paper_share`;
CREATE TABLE `paper_share` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exampaperid` int(11) DEFAULT NULL,
  `teacherid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of paper_share
-- ----------------------------

-- ----------------------------
-- Table structure for paper_title_model
-- ----------------------------
DROP TABLE IF EXISTS `paper_title_model`;
CREATE TABLE `paper_title_model` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of paper_title_model
-- ----------------------------
INSERT INTO `paper_title_model` VALUES ('1', '杭州师范大学国服期末考卷', '杭州师范大学 学院。。。看最后怎么代替空白的');
INSERT INTO `paper_title_model` VALUES ('2', 'string', 'string');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rolename` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `creatorid` int(11) DEFAULT NULL,
  `available` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of role
-- ----------------------------

-- ----------------------------
-- Table structure for school
-- ----------------------------
DROP TABLE IF EXISTS `school`;
CREATE TABLE `school` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `universityid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `userdefaultpwd` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of school
-- ----------------------------
INSERT INTO `school` VALUES ('1', '杭州国际服务工程学院', '1', '1', null);
INSERT INTO `school` VALUES ('2', '理学院', '1', '1', null);

-- ----------------------------
-- Table structure for student_info
-- ----------------------------
DROP TABLE IF EXISTS `student_info`;
CREATE TABLE `student_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `departmentid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `majorid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of student_info
-- ----------------------------
INSERT INTO `student_info` VALUES ('1', null, null, null, '1', null);
INSERT INTO `student_info` VALUES ('2', null, '1', '1', '1', '1');
INSERT INTO `student_info` VALUES ('3', '5', '1', '1', '1', '1');
INSERT INTO `student_info` VALUES ('4', '7', '1', '1', '1', '1');
INSERT INTO `student_info` VALUES ('5', '8', '1', '1', '1', '1');
INSERT INTO `student_info` VALUES ('6', null, '1', '1', '1', '1');
INSERT INTO `student_info` VALUES ('7', '9', '1', '1', '1', '1');
INSERT INTO `student_info` VALUES ('8', null, '1', '1', '1', '1');
INSERT INTO `student_info` VALUES ('9', '10', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for teacher_info
-- ----------------------------
DROP TABLE IF EXISTS `teacher_info`;
CREATE TABLE `teacher_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `schoolid` int(11) DEFAULT NULL,
  `departmentid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of teacher_info
-- ----------------------------
INSERT INTO `teacher_info` VALUES ('2', '4', '1', '1');
INSERT INTO `teacher_info` VALUES ('3', null, '1', '1');
INSERT INTO `teacher_info` VALUES ('4', '6', null, null);

-- ----------------------------
-- Table structure for university
-- ----------------------------
DROP TABLE IF EXISTS `university`;
CREATE TABLE `university` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `acronym` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of university
-- ----------------------------
INSERT INTO `university` VALUES ('1', '杭州师范大学', 'hznu');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `universityid` int(11) DEFAULT NULL COMMENT '学校id',
  `schoolid` int(11) DEFAULT NULL COMMENT '学院id',
  `username` varchar(30) COLLATE utf8_bin NOT NULL COMMENT '用户名',
  `password` varchar(50) COLLATE utf8_bin NOT NULL,
  `salt` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '加盐',
  `workid` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '用户号',
  `sex` int(11) DEFAULT NULL,
  `usertype` int(11) DEFAULT NULL,
  `realname` char(50) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', '1', 'system', '5e6722234ca236f0834b6183b0952f53', '44f27da0f4068167', '11111', '1', '5', '国服');
INSERT INTO `user` VALUES ('4', '1', '1', 'teacher', '5e6722234ca236f0834b6183b0952f53', '44f27da0f4068167', '11100', '1', '1', '国服教师');
INSERT INTO `user` VALUES ('5', '1', '1', '2016210402056', '22239115b547812108bbb6ba64ca069f', '74d50b20efb124c0', '2016210402056', '2', '2', '吴蓉');
INSERT INTO `user` VALUES ('6', null, null, 'teacher2', '96f24fcef35ca86d4e36ee0e8d1dfa32', '2a9f6db9f4d06423', null, null, '1', '测试');
INSERT INTO `user` VALUES ('7', '1', '1', '2016210402066', '96f24fcef35ca86d4e36ee0e8d1dfa32', '2a9f6db9f4d06423', '2016210402066', '1', '2', '吴琦东');
INSERT INTO `user` VALUES ('8', '1', '1', '2016210402060', '22239115b547812108bbb6ba64ca069f', '2a9f6db9f4d06423', '2016210402060', '1', '2', 'jyb');
INSERT INTO `user` VALUES ('9', '1', '1', '2016210402055', 'f5fc3b7d0b529f5f0a54d98b6fa16e87', 'a3f824a0222e502e', '2016210402055', '2', '2', '诺诺');
INSERT INTO `user` VALUES ('10', null, null, '2016210402057', '3b09781911a20b5a836daef95147d222', '31105b5ccb58f2f3', null, null, '2', '王媛媛');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `roleid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of user_role
-- ----------------------------
